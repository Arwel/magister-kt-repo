<?php

use App\Jobs\AvatarGenerateJob;
use App\Role;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class InitialUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminRole = Role::create([
            'title' => __('roles.admin'),
            'priority' => 1000,
            'removable' => false,
        ]);

        $admin = User::create([
            'name' => 'Admin',
            'email' => 'admin@website.com',
            'password' => Hash::make('admin01'),
            'avatar' => '',
            'role_id' => $adminRole->id,
        ]);
        AvatarGenerateJob::dispatch($admin)->onQueue('database');

        Role::insert([
            [
                'title' => __('roles.moder'),
                'priority' => 100,
                'removable' => false,
            ],
            [
                'title' => __('roles.user'),
                'priority' => 1,
                'removable' => false,
            ],
            [
                'title' => __('roles.banned'),
                'priority' => -1,
                'removable' => false,
            ],
        ]);
    }
}
