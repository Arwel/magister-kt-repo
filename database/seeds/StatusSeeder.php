<?php

use App\Status;
use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Status::insert([
            [
                'title' => __('auth.active'),
            ],
            [
                'title' => __('auth.pending'),
            ],
            [
                'title' => __('auth.deleted'),
            ],
            [
                'title' => __('auth.banned'),
            ],
            [
                'title' => __('auth.private'),
            ],
            [
                'title' => __('auth.rejected'),
            ],
        ]);
    }
}
