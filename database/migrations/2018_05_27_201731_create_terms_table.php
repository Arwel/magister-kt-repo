<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTermsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('terms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 50)->unique();
            $table->string('slug', 50)->unique();
            $table->string('lang')->default(config('app.locale'));
            $table->text('description')->nullable();
            $table->string('thumbnail_url')->nullable();
            $table->integer('term_id')->unsigned()->nullable();
            $table->integer('taxonomy_id')->unsigned();
            $table->timestamps();

            $table->foreign('taxonomy_id')->references('id')->on('taxonomies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('terms');
    }
}
