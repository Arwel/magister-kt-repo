<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('works', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->uniquie();
            $table->string('slug');
            $table->text('description');
            $table->string('work_url');
            $table->float('plagiarism_percent')->nullable();
            $table->string('lang')->default(config('app.locale'));
            $table->string('thumbnail_url')->nullable();
            $table->integer('status_id')->unsigned()->default(2);
            $table->integer('user_id')->unsigned();
            $table->integer('year')->unsigned();
            $table->timestamps();

            $table->foreign('status_id')->references('id')->on('statuses');
            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::create('previews', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('work_id')->unsigned();
            $table->string('url');
            $table->timestamps();

            $table->foreign('work_id')->references('id')->on('works');
        });

        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id');
            $table->text('text');
            $table->integer('user_id')->unsigned();
            $table->integer('work_id')->unsigned();
            $table->integer('status_id')->unsigned()->default(2);
            $table->integer('comment_id')->unsigned()->default(0);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('work_id')->references('id')->on('works');
            $table->foreign('status_id')->references('id')->on('statuses');
        });

        Schema::create('work_term', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('work_id')->unsigned();
            $table->integer('term_id')->unsigned();
            $table->timestamps();

            $table->foreign('work_id')->references('id')->on('works');
            $table->foreign('term_id')->references('id')->on('terms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_term');
        Schema::dropIfExists('comments');
        Schema::dropIfExists('previews');
        Schema::dropIfExists('works');
    }
}
