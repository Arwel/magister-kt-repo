<?php

use App\Jobs\AvatarGenerateJob;
use App\Role;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes([
    'register' => false,
]);

Route::any('/test', function () {

    AvatarGenerateJob::dispatch(Auth::user())->onQueue('database');

});

// public
Route::any('/', 'WorkController@index')->name('home');
Route::any('/work/{slug}', 'WorkController@show')->name('public-work-show');
Route::any('/author/works/{user}', 'WorkController@byAuthor')->name('public-works-user');
Route::any('/year/works/{year}', 'WorkController@byYear')->name('public-works-year');
Route::any('/term/works/{term}', 'WorkController@byTerm')->name('public-works-term');
Route::any('/search/works', 'WorkController@bySearch')->name('public-works-search');

// static pages
Route::get('/about', 'PagesController@about')->name('about');
Route::get('/contact-us', 'PagesController@contacts')->name('contacts');
Route::post('/contact-us', 'PagesController@ajaxContactUs')->name('contact-us-send');
Route::get('/404', 'PagesController@error404')->name('error-404');
Route::get('/403', 'PagesController@error403')->name('error-403');

// guests only

Route::group(['middleware' => ['web', 'guest']], function () {
    Route::get('/request-access', 'RequestAccessController@create')->name('request-access');
    Route::post('/request-access', 'RequestAccessController@store')->name('request-access-store');

    Route::get('/invite/{token}', 'Auth\RegisterController@inviteForm')->name('invite');
    Route::post('/user/invite', 'Auth\RegisterController@inviteStore')->name('invite-store');
});

// admin only

Route::group(['prefix' => 'admin', 'middleware' => ['web', 'auth', 'admin', 'no-ban']], function () {
    // access requests
    Route::get('/requests', 'RequestAccessController@index')->name('admin-request-access-list');
    Route::post('/request/{request}/remove', 'RequestAccessController@remove')->name('request-access-remove');
    Route::post('/request/{request}/invite', 'RequestAccessController@sendInvite')->name('request-access-invite');
    Route::post('/request-access', 'RequestAccessController@store')->name('admin-request-access-store');

    // users
    Route::get('/users', 'Admin\UsersController@index')->name('admin-users');

    Route::get('/user/create', 'Admin\UsersController@create')->name('admin-user-create');
    Route::post('/user/create', 'Admin\UsersController@store')->name('admin-user-store');

    Route::get('/user/{user}/edit', 'Admin\UsersController@edit')->name('admin-user-edit');
    Route::post('/user/{user}/edit', 'Admin\UsersController@update')->name('admin-user-update');

    Route::post('/user/{user}/remove', 'Admin\UsersController@remove')->name('admin-user-remove');

    // logs
    Route::get('/logs', 'Admin\LogController@index')->name('admin-logs');
    Route::post('/log/{log}/remove', 'Admin\LogController@remove')->name('admin-log-remove');
    Route::post('/log/clear', 'Admin\LogController@clear')->name('admin-log-clear');

    // jobs
    Route::get('/jobs', 'Admin\JobController@index')->name('admin-jobs');
    Route::post('/job/{job}/remove', 'Admin\JobController@remove')->name('admin-job-remove');
    Route::post('/job/clear', 'Admin\JobController@clear')->name('admin-job-clear');
});

// moders & admins only
Route::group(['prefix' => 'moderate', 'middleware' => ['web', 'auth', 'moder', 'no-ban',]], function () {
    // works
    Route::get('/works/{filter?}', 'Moder\WorkController@index')->name('moder-works');

    Route::get('/work/create', 'Moder\WorkController@create')->name('moder-work-create');
    Route::post('/work/create', 'Moder\WorkController@store')->name('moder-work-store');

    Route::get('/work/{work}/edit', 'Moder\WorkController@edit')->name('moder-work-edit');
    Route::post('/work/{work}/edit', 'Moder\WorkController@update')->name('moder-work-update');

    Route::post('/work/{work}/remove', 'Moder\WorkController@remove')->name('moder-work-remove');

    Route::post('/work/{work}/validate', 'Moder\WorkController@ajaxValidatePlagiat')->name('moder-work-validate');

    Route::post('/work/{work}/generate-preview', 'Moder\WorkController@generatePreview')->name('moder-work-generate-preview');

    // taxonomies
    Route::get('/taxonomies', 'Moder\TaxonomyController@index')->name('moder-taxonomies');

    Route::get('/taxonomy/create', 'Moder\TaxonomyController@create')->name('moder-taxonomy-create');
    Route::post('/taxonomy/create', 'Moder\TaxonomyController@store')->name('moder-taxonomy-store');

    Route::get('/taxonomy/{taxonomy}/edit', 'Moder\TaxonomyController@edit')->name('moder-taxonomy-edit');
    Route::post('/taxonomy/{taxonomy}/edit', 'Moder\TaxonomyController@update')->name('moder-taxonomy-update');

    Route::post('/taxonomy/{taxonomy}/remove', 'Moder\TaxonomyController@remove')->name('moder-taxonomy-remove');

    // terms
    Route::get('/{taxonomy}/terms', 'Moder\TermController@index')->name('moder-terms');

    Route::get('/{taxonomy}/term/create', 'Moder\TermController@create')->name('moder-term-create');
    Route::post('/{taxonomy}/term/create', 'Moder\TermController@store')->name('moder-term-store');

    Route::get('/{taxonomy}/term/{term}/edit', 'Moder\TermController@edit')->name('moder-term-edit');
    Route::post('/{taxonomy}/term/{term}/edit', 'Moder\TermController@update')->name('moder-term-update');

    Route::post('/{taxonomy}/term/{term}/remove', 'Moder\TermController@remove')->name('moder-term-remove');

    // comments
    Route::get('/comments', 'Moder\CommentController@index')->name('moder-comments');

    Route::post('/{work}/comment/create', 'Moder\CommentController@store')->name('moder-comment-store');

    Route::post('/comment/{comment}/edit', 'Moder\CommentController@update')->name('moder-comment-update');

    Route::post('/comment/{comment}/remove', 'Moder\CommentController@remove')->name('moder-comment-remove');

    // download requesta
    Route::get('/download-reqests', 'Moder\DownloadRequestController@index')->name('moder-download-requests');

    Route::post('/download-reqest/{request}/edit', 'Moder\DownloadRequestController@update')->name('moder-download-request-update');

    Route::post('/download-reqest/{request}/remove', 'Moder\DownloadRequestController@remove')->name('moder-download-request-remove');
});

// for all authenicated users
Route::group(['prefix' => 'user', 'middleware' => ['web', 'auth', 'no-ban', ]], function () {
    // works
    Route::get('/works', 'User\WorkController@index')->name('user-works');

    Route::get('/work/create', 'User\WorkController@create')->name('user-work-create');
    Route::post('/work/create', 'User\WorkController@store')->name('user-work-store');

    Route::get('/work/{work}/edit', 'User\WorkController@edit')->name('user-work-edit');
    Route::post('/work/{work}/edit', 'User\WorkController@update')->name('user-work-update');

    Route::post('/work/{work}/remove', 'User\WorkController@remove')->name('user-work-remove');

    // comments
    Route::post('/{work}/comment/create/{status?}', 'User\CommentController@store')->name('user-comment-store');

    Route::post('/comment/{comment}/edit', 'User\CommentController@update')->name('user-comment-update');

    Route::post('/comment/{comment}/remove', 'User\CommentController@remove')->name('user-comment-remove');

    // user
    Route::get('/dashboard', 'User\UserController@edit')->name('user-user-edit');
    Route::post('/dashboard', 'User\UserController@update')->name('user-user-update');

    // download requests
    Route::post('/download-reqest/{work}/create', 'User\DownloadRequestController@store')->name('user-download-request-store');
    Route::post('/download-reqest/{work}/download', 'User\DownloadRequestController@download')->name('user-download-request-download');
    Route::post('/download-reqest/{request}/edit', 'User\DownloadRequestController@update')->name('user-download-request-update');

    Route::post('/download-reqest/{request}/remove', 'User\DownloadRequestController@remove')->name('user-download-request-remove');
});

