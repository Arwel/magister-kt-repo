<?php
use Illuminate\Support\Facades\Auth;

return [
    'home' => [
        'title' => 'nav.home',
    ],
    'home' => [
        'title' => 'nav.repo',
        'test' => 'public-',
        'strict' => true,
    ],
    'about' => [
        'title' => 'nav.about',
    ],
    'contacts' => [
        'title' => 'nav.contacts',
    ],

    // only for guests
    'login' => [
        'title' => 'nav.login',
        'check' => function () {
            return !Auth::user();
        },
    ],
    'request-access' => [
        'title' => 'nav.request-access',
        'check' => function () {
            return !Auth::user();
        },
    ],

    // only for users
    'user' => [
        'title' => 'nav.repo',
        'test' => 'user-',
        'strict' => true,
    ],
];
