(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["glide-slider"],{

/***/ "./ts/components/etc/glide-slider.ts":
/*!*******************************************!*\
  !*** ./ts/components/etc/glide-slider.ts ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _glidejs_glide__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @glidejs/glide */ "./node_modules/@glidejs/glide/dist/glide.esm.js");
var __values = (undefined && undefined.__values) || function (o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
};

/* harmony default export */ __webpack_exports__["default"] = (function (context) {
    if (context === void 0) { context = document.body; }
    var e_1, _a;
    var _loop_1 = function (slider) {
        if (slider.querySelectorAll('.glide__slide').length < 1) {
            return "continue";
        }
        var glide = new _glidejs_glide__WEBPACK_IMPORTED_MODULE_0__["default"](slider, {
            type: 'carousel',
            gap: 0,
            autoplay: 4000,
            hoverpause: true
        });
        glide.mount();
        var scrollDown = slider.querySelector('[data-role="scroll-down"]');
        scrollDown && scrollDown.addEventListener('click', function () {
            window.scroll({
                top: slider.getBoundingClientRect().height,
                left: 0,
                behavior: 'smooth'
            });
        });
    };
    try {
        for (var _b = __values(Array.from(context.querySelectorAll('[data-component="glide-slider"]'))), _c = _b.next(); !_c.done; _c = _b.next()) {
            var slider = _c.value;
            _loop_1(slider);
        }
    }
    catch (e_1_1) { e_1 = { error: e_1_1 }; }
    finally {
        try {
            if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
        }
        finally { if (e_1) throw e_1.error; }
    }
});


/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi90cy9jb21wb25lbnRzL2V0Yy9nbGlkZS1zbGlkZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFtQztBQUVwQix5RUFBQyxPQUF1QjtJQUF2QixvQ0FBVSxRQUFRLENBQUMsSUFBSTs7NEJBQ3hCLE1BQU07UUFDYixJQUFJLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxlQUFlLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFOztTQUV4RDtRQUVELElBQU0sS0FBSyxHQUFHLElBQUksc0RBQUssQ0FBQyxNQUFNLEVBQUU7WUFDNUIsSUFBSSxFQUFFLFVBQVU7WUFDaEIsR0FBRyxFQUFFLENBQUM7WUFDTixRQUFRLEVBQUUsSUFBSTtZQUNkLFVBQVUsRUFBRSxJQUFJO1NBQ25CLENBQUMsQ0FBQztRQUNILEtBQUssQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUVkLElBQU0sVUFBVSxHQUFHLE1BQU0sQ0FBQyxhQUFhLENBQUMsMkJBQTJCLENBQUMsQ0FBQztRQUNyRSxVQUFVLElBQUksVUFBVSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sRUFBRTtZQUMvQyxNQUFNLENBQUMsTUFBTSxDQUFDO2dCQUNaLEdBQUcsRUFBRSxNQUFNLENBQUMscUJBQXFCLEVBQUUsQ0FBQyxNQUFNO2dCQUMxQyxJQUFJLEVBQUUsQ0FBQztnQkFDUCxRQUFRLEVBQUUsUUFBUTthQUNuQixDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7O1FBckJELEtBQXFCLHVCQUFLLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxpQ0FBaUMsQ0FBQyxDQUFDO1lBQXZGLElBQU0sTUFBTTtvQkFBTixNQUFNO1NBcUJoQjs7Ozs7Ozs7O0FBQ0wsQ0FBQyxFQUFDIiwiZmlsZSI6ImdsaWRlLXNsaWRlci5tb2R1bGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgR2xpZGUgZnJvbSAnQGdsaWRlanMvZ2xpZGUnO1xuXG5leHBvcnQgZGVmYXVsdCAoY29udGV4dCA9IGRvY3VtZW50LmJvZHkpID0+IHtcbiAgICBmb3IgKGNvbnN0IHNsaWRlciBvZiBBcnJheS5mcm9tKGNvbnRleHQucXVlcnlTZWxlY3RvckFsbCgnW2RhdGEtY29tcG9uZW50PVwiZ2xpZGUtc2xpZGVyXCJdJykpKSB7XG4gICAgICAgIGlmIChzbGlkZXIucXVlcnlTZWxlY3RvckFsbCgnLmdsaWRlX19zbGlkZScpLmxlbmd0aCA8IDEpIHtcbiAgICAgICAgICAgIGNvbnRpbnVlO1xuICAgICAgICB9XG5cbiAgICAgICAgY29uc3QgZ2xpZGUgPSBuZXcgR2xpZGUoc2xpZGVyLCB7XG4gICAgICAgICAgICB0eXBlOiAnY2Fyb3VzZWwnLFxuICAgICAgICAgICAgZ2FwOiAwLFxuICAgICAgICAgICAgYXV0b3BsYXk6IDQwMDAsXG4gICAgICAgICAgICBob3ZlcnBhdXNlOiB0cnVlXG4gICAgICAgIH0pO1xuICAgICAgICBnbGlkZS5tb3VudCgpO1xuXG4gICAgICAgIGNvbnN0IHNjcm9sbERvd24gPSBzbGlkZXIucXVlcnlTZWxlY3RvcignW2RhdGEtcm9sZT1cInNjcm9sbC1kb3duXCJdJyk7XG4gICAgICAgIHNjcm9sbERvd24gJiYgc2Nyb2xsRG93bi5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsICgpID0+IHtcbiAgICAgICAgICAgIHdpbmRvdy5zY3JvbGwoe1xuICAgICAgICAgICAgICB0b3A6IHNsaWRlci5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKS5oZWlnaHQsXG4gICAgICAgICAgICAgIGxlZnQ6IDAsXG4gICAgICAgICAgICAgIGJlaGF2aW9yOiAnc21vb3RoJ1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH1cbn07XG4iXSwic291cmNlUm9vdCI6IiJ9