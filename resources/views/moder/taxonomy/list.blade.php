@extends('layouts.main')

<?php
use App\Utils\Helpers;
use App\Utils\Form;
?>

@section('js-settings')
window.app.listAnimator = true;
@endsection

@section('content')

<section class="page bg-color_light">
      <div class="row section-head mb-35">
         <div class="col full">
            <h1>@lang('content.taxonomies')</h1>
         </div>
      </div>

      <!-- Blog Entries -->
      <div class="blog-entries row list">

        @foreach($taxonomies as $taxonomy)
            <!-- Entry -->
         <article class="entry width_full list-item">

            <div class=" entry-header display_flex width_full justify-content_space-between p-10 align-items_center">

               <div class=" entry-title">
                  <h3><a href="{{ route('moder-taxonomy-edit', ['taxonomy' => $taxonomy]) }}">{{ $taxonomy->title }}</a></h3>
                  <p>
                    {{ Helpers::cropText($taxonomy->description) }}
                  </p>
               </div>

               <div class="">
                  <div class="post-meta display_flex align-items_center">
                    <a href="{{ route('moder-terms', ['taxonomy' => $taxonomy]) }}" class="mr-10">@lang('nav.terms')</a>
                    <a href="{{ route('moder-term-create', ['taxonomy' => $taxonomy]) }}" class="mr-10">@lang('nav.create-term')</a>

                    <?php
                    echo Form::start(route('moder-taxonomy-remove', ['taxonomy' => $taxonomy]), 'mb-0', 'POST', [
                      'data-modifer' => 'confirmator',
                    ]);
                    echo Form::submit('<span class="hint__text">'.__('forms.remove').'</span>', 'button-small button-danger button-icon fas fa-trash hint__holder');
                    echo Form::end();
                    ?>

                  </div>
               </div>

            </div>

         </article> <!-- Entry End -->

        @endforeach

      </div> <!-- Blog Entries End -->

      {{ $taxonomies->links() }}

    </section>


@endsection
