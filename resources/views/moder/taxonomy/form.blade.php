@extends('layouts.main')

<?php
use App\Utils\Form;
?>

@section('content')

<section class="page bg-color_light user-works-form">
      <div class="row">
        <div class="col">

        <div class="box box-primary create-form mb-35" >

        <div class="box-header with-border create-form__header">
          <h1 class="box-title create-form__header-title mb-35">
            @if($taxonomy)
              @lang('forms.taxonomy'):&nbsp;{{ $taxonomy->title }}
            @else
              {{ __('forms.new-taxonomy') }}
            @endif
          </h1>

          <div class="display_flex create-form__header-actions">

            @if($taxonomy)
              <a href="{{ route('moder-terms', ['taxonomy' => $taxonomy]) }}" class="mr-15 ">@lang('nav.terms')</a>
              <a href="{{ route('moder-term-create', ['taxonomy' => $taxonomy]) }}" class="mr-15 ">@lang('nav.create-term')</a>
              <a href="{{ route('moder-taxonomy-create') }}" class="mr-15 ">{{ __('forms.create-more') }}</a>
            @endif

            <a href="{{ route('moder-taxonomies') }}" class=" ">{{ __('forms.list') }}</a>
          </div>
          <?php
          if ($taxonomy) {
            echo Form::start(route('moder-taxonomy-remove', ['taxonomy' => $taxonomy]), '', 'POST', [
              'data-modifer' => 'confirmator',
            ]);
            echo Form::submit('<span class="hint__text">'.__('forms.remove').'</span>', 'button-small button-danger button-icon fas fa-trash hint__holder');
            echo Form::end();
          }
          ?>
        </div>
        <!-- /.box-header -->
        <?php
        echo Form::start($route, 'box-body');
        echo Form::input('title', __('forms.title'), $taxonomy);
        echo Form::textarea('description', __('forms.description'), $taxonomy);
        echo '<div class="form-group">';
        echo Form::submit(__('forms.save'), 'btn btn-success pull-right');
        echo '</div>';
        echo Form::end();
        ?>
        <!-- /.box-body -->


        <!-- /.box-footer -->
    </div>


        </div>
      </div>
    </section>


@endsection
