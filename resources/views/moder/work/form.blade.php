@extends('layouts.main')

<?php
use App\Utils\Form;

$lang = $work ? $work->lang : old('lang');
?>

@section('js-settings')
window.app.confirmator = true;
@endsection

@section('content')

<section class="page bg-color_light user-works-form">
  <div class="row">
    <div class="col">

      <div class="box box-primary create-form mb-35" >

        <div class="box-header with-border create-form__header">
          <h1 class="box-title create-form__header-title mb-35"> {{ __('forms.work') }}
            @if($work)
            <span class="color_light bg-color_dark font-weight_bold pw-15">{{ $work->status->title }}</span>
            @endif
          </h1>

          <div class="display_flex create-form__header-actions">

            @if($work)
            <a href="{{ route('public-work-show', ['slug' => $work->slug]) }}" class="mr-15">@lang('forms.show')</a>
            <a href="{{ route('moder-work-create') }}" >{{ __('forms.create-more') }}</a>
            @endif

            <a href="{{ route('moder-works') }}" class=" ml-15 ">{{ __('forms.list') }}</a>
          </div>
          <?php
          if ($work) {
            echo '<div class="display_flex">';

            echo Form::start(route('moder-work-remove', ['work' => $work]), 'mr-15', 'POST', [
              'data-modifer' => 'confirmator',
            ]);
            echo Form::submit('<span class="hint__text">'.__('forms.remove').'</span>', 'button-small button-danger button-icon fas fa-trash hint__holder');
            echo Form::end();

            echo Form::start(route('moder-work-validate', ['work' => $work]), 'mr-15');
            echo Form::submit('<span class="hint__text">'.__('forms.plagiarism-check').'</span>', 'button-small button-primary button-icon fas fa-clipboard-check hint__holder');
            echo Form::end();

            echo Form::start(route('moder-work-generate-preview', ['work' => $work]), 'mr-15');
            echo Form::submit('<span class="hint__text">'.__('forms.generate-preview').'</span>', 'button-small button-outline-dark button-icon fas fa-eye hint__holder');
            echo Form::end();



            echo Form::start(route('user-download-request-download', ['work' => $work]), '');
            echo Form::submit('<span class="hint__text">'.__('forms.download').'</span>', 'button-small button-outline-dark button-icon fas fa-download hint__holder');
            echo Form::end();

            echo '</div>';
          }
          ?>
        </div>
        <!-- /.box-header -->
        <?php
        echo Form::start($route, 'box-body mt-35', 'POST', [
          'enctype' => 'multipart/form-data',
        ]);
        echo Form::input('title', __('forms.title'), $work);
        echo Form::textarea('description', __('forms.description'), $work);

        echo Form::select('status_id', __('forms.status'), $statuses, $work ? $work->status_id : old('status_id'));

        ?>
        <div class="">
          <h4>@lang('forms.lang')</h4>
          <select name="lang">
            @foreach(config('app.supportedLanguages') as $value)
            <option
            value="{{ $value }}"
            {{ ($lang == $value ? 'selected="selected"' : '') }}>{{ $value }}</option>
            @endforeach
          </select>
        </div>
        <?php

        echo '<div class="display_flex work-attachments"><div class="work-attachment">';
        // work attachment
        if ($work) {
          echo '<b>'.basename($work->work_url).'</b>';
        }
        echo Form::file('work_url', __('forms.attached'), $work) . '</div>';
        // !work attachment

        // work thumbnail
        echo '<div class="work-attachment__spacer"></div><div class="work-attachment">';
        if ($work) {
          echo '<div class="mt-24"><img src="'.$work->getThumbnailUrl().'" class="max-width_150"></div>';
        }
        echo Form::file('thumbnail_url', __('forms.thumbnail'), $work) . '</div></div>';





        // terms
        foreach ($taxonomies as $taxonomy) {
          if ($taxonomy->termsLocale->isEmpty()) {
            continue;
          }

          echo '<div>';
          echo Form::select('terms['.$taxonomy->id.']', $taxonomy->title, $taxonomy->termsLocale, $work ? $work->getTermsId() : old('terms['.$taxonomy->id.']'), false, true);
          echo '</div>';
        }

        echo '<div class="form-group">';
        echo Form::submit(__('forms.save'), 'btn btn-success pull-right');
        echo '</div>';
        echo Form::end();
        ?>
        <!-- /.box-body -->


        <!-- /.box-footer -->
      </div>

      <div>

        <?php
        if ($work) {
          if ($comments->isNotEmpty()) {
            echo '<h4>'.__('forms.comments').'</h4>';
          }

          foreach ($comments as $comment) {
            echo '
            <!-- Entry -->
            <article class="entry width_full list-item p-0">
            <div class="author-image " style="background-image: url('.$comment->user->getAvatar().');"></div>

            <div class=" list-item-content width_full display_flex  justify-content_space-between">

            <div class="g-9 entry-title">
            <h3>'.$comment->user->name.' <span class="color_light bg-color_dark font-weight_bold pw-15">'.$comment->user->getLabel().'</span></h3>
            <p>
            '.$comment->text.'
            </p>
            </div>

            <div class="display_flex align-items_center">
              <div class="post-meta">
              '.
              Form::start(route('moder-comment-remove', ['comment' => $comment]), 'm-0', 'POST', [
              'data-modifer' => 'confirmator',
              ]).
              Form::submit('', 'button-small button-danger button-icon fas fa-trash').
              Form::end().
              '
              </div>
              </div>

            </div>

            </article> <!-- Entry End -->
            ';
          }

          if (!$comments->isEmpty()) {
            echo '<hr>';
          }


          echo Form::start(route('moder-comment-store', ['work' => $work]), 'box-body');
          echo Form::textarea('text', __('forms.comment'), null);
          echo Form::input('email', __('forms.send-to-email'), null, false, 'checkbox', 'mb-11', 'display_flex flex-direction_row-reverse justify-content_flex-end align-items_center');
          echo '<div class="form-group">';
          echo Form::submit(__('forms.post'), 'btn btn-success pull-right');
          echo '</div>';
          echo Form::end();

          if ($comments->hasMorePages()) {
            echo $comments->links();
          }
        }
        ?>
      </div>


    </div>
  </div>
</section>


@endsection
