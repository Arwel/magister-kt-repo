<h1>{{ $work->title }}</h1>
<a href="{{ route('public-work-show', ['work' => $work]) }}">@lang('forms.show')</a>

@if ($error)
    <div class="color_danger">
       {{ $error }}
    </div>
@endif

@if (!$error)
    <h4>@lang('forms.validation-result')</h4>
    <div>
    <?php
    if ($result < 30) {
        echo '<h5 class="color_success">'.__('forms.validation-pass').' '.$result.'% '.__('forms.copied').'</h5>';
    } elseif ($result < 50) {
        echo '<h5 class="color_warning">'.__('forms.validation-warning').' '.$result.'% '.__('forms.copied').'</h5>';
    } else {
        echo '<h5 class="color_danger">'.__('forms.validation-failed').' '.$result.'% '.__('forms.copied').'</h5>';
    }
    ?>
</div>
@endif

<p>
    {!! $work->description !!}
</p>