@extends('layouts.main')

<?php
use App\Utils\Helpers;
?>

@section('js-settings')
window.app.listAnimator = true;
@endsection

@section('content')
  <section class="page bg-color_light">
      <div class="row section-head mb-35">
         <div class="col full">
            <h1>@lang('content.works-for-moderate')</h1>
            <a href="{{ route('moder-works', ['filter' => 'all']) }}">@lang('content.show-all')</a>
         </div>
         <div>

         </div>
      </div>

      <!-- Blog Entries -->
      <div class="blog-entries row list">

        @foreach($works as $work)
            <!-- Entry -->
         <article class="entry width_full list-item p-0">
          <div class="author-image " style="background-image: url('{{ $work->getThumbnailUrl() }}');"></div>

          <div class="list-item-content width_full">
            <div class=" entry-title width_full display_flex justify-content_space-between">
              <div class="">
                  <h3><a href="{{ route('moder-work-edit', ['work' => $work]) }}">{{ $work->title }}</a></h3>
                  <p>
                    {{ Helpers::cropText($work->description) }}
                  </p>
                </div>
                <div class="post-meta">
                     <time pubdate="" class="post-date" datetime="{{ $work->getCreatedDateFormatted() }}">{{ $work->getCreatedDateFormatted() }}</time>
                     @if ($work->user)
                     <div class="color-dark_darken3 font-weight_bold">{{ $work->user->name }}</div>
                     @endif
                  </div>
               </div>


              <hr>
              <div>
                @if ($work->previews->isNotEmpty())
                <i class="fas fa-eye color_accent display_inline-block mr-10 hint__holder">
                  <span class="hint__text">@lang('forms.preview-generated')</span>
                </i>
                @else
                <i class="fas fa-eye  display_inline-block mr-10 hint__holder">
                  <span class="hint__text">@lang('forms.preview-generated')</span>
                </i>
                @endif

                @if ($work->plagiarism_percent > 0)
                <i class="fas fa-copy color_accent  display_inline-block mr-5 hint__holder">
                  <span class="hint__text">@lang('forms.plagiarism-check')</span>
                </i>
                @else
                <i class="fas fa-copy   display_inline-block mr-5 hint__holder">
                  <span class="hint__text">@lang('forms.plagiarism-check')</span>
                </i>
                @endif
                {{ (float)$work->plagiarism_percent }}%
              </div>
          </div>
         </article> <!-- Entry End -->

        @endforeach

      </div> <!-- Blog Entries End -->

      {{ $works->links() }}

    </section>

@endsection
