@extends('layouts.main')

<?php
use App\Utils\Form;
use App\Utils\Helpers;
?>

@section('js-settings')
window.app.listAnimator = true;
@endsection

@section('content')

<section class="page bg-color_light">
      <div class="row section-head mb-35">
         <div class="col full">
            <h1>@lang('content.terms-for-tax'):&nbsp;{{ $taxonomy->title }}</h1>
            <a href="{{ route('moder-term-create', ['taxonomy' => $taxonomy]) }}">@lang('nav.create-term')</a>
         </div>
      </div>

      <!-- Blog Entries -->
      <div class="blog-entries row list">

        @foreach($terms as $term)
            <!-- Entry -->
         <article class="entry width_full list-item">

            <div class=" entry-header display_flex justify-content_space-between p-10 align-items_center">

               <div class="entry-title">
                  <h3><a href="{{ route('moder-term-edit', ['taxonomy' => $taxonomy, 'term' => $term]) }}">{{ $term->title }}</a></h3>
                  <p>{{ Helpers::cropText($term->description) }}</p>
               </div>

               <div class="">
                  <div class="post-meta">
                      <?php
                      echo Form::start(route('moder-term-remove', ['taxonomy' => $taxonomy, 'term' => $term]), 'mb-0', 'POST', [
                        'data-modifer' => 'confirmator',
                      ]);
                      echo Form::submit('<span class="hint__text">'.__('forms.remove').'</span>', 'button-small button-danger button-icon fas fa-trash hint__holder');
                      echo Form::end();
                      ?>
                  </div>
               </div>

            </div>

         </article> <!-- Entry End -->

        @endforeach
      </div> <!-- Blog Entries End -->

        {{ $terms->links() }}

    </section>


@endsection
