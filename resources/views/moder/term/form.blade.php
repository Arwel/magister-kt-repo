@extends('layouts.main')

<?php
use App\Utils\Form;
?>

@section('content')

<section class="page bg-color_light user-works-form">
      <div class="row">
        <div class="col">

        <div class="box box-primary create-form mb-35" >

        <div class="box-header with-border create-form__header">
          <h1 class="box-title create-form__header-title mb-35">
            @if($term)
              @lang('forms.term'):&nbsp;{{ $term->title }}
            @else
              {{ __('forms.new-term') }}
            @endif
          </h1>

          <div class="display_flex create-form__header-actions">

            @if($term)
              <a href="{{ route('moder-term-create', ['taxonomy' => $taxonomy]) }}" class=" mr-15">{{ __('forms.create-more') }}</a>
            @endif

            <a href="{{ route('moder-terms', ['taxonomy' => $taxonomy]) }}" >{{ __('forms.list') }}</a>
          </div>
          <?php
            if ($term) {
              echo Form::start(route('moder-term-remove', ['taxonomy' => $taxonomy, 'term' => $term]), '', 'POST', [
                'data-modifer' => 'confirmator',
              ]);
              echo Form::submit('<span class="hint__text">'.__('forms.remove').'</span>', 'button-small button-danger button-icon fas fa-trash hint__holder');
              echo Form::end();
            }
            ?>
        </div>
        <!-- /.box-header -->
         <?php
        echo Form::start($route, 'box-body', 'POST', [
            'enctype' => 'multipart/form-data',
        ]);
        echo Form::input('title', __('forms.title'), $term);
        echo Form::textarea('description', __('forms.description'), $term);

        if ($term && !empty($term->thumbnail_url)) {
          echo '<div><img src="'.$term->getThumbnailUrl().'" class="max-width_150"></div>';
        }

        echo Form::file('thumbnail_url', __('forms.thumbnail'), $term);
        echo Form::select('term_id', __('forms.parent'), $termOptions, $term ? $term->term_id : old('term_id'), true);
        echo '<div class="form-group">';
        echo Form::submit(__('forms.save'), 'btn btn-success pull-right');
        echo '</div>';
        echo Form::end();
        ?>
        <!-- /.box-body -->


        <!-- /.box-footer -->
    </div>


        </div>
      </div>
    </section>


@endsection
