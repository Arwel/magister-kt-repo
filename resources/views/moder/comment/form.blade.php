@extends('layouts.main')

<?php
use App\Utils\Form;
?>

@section('content')
    <div class="box box-primary create-form" >

        <div class="box-header with-border create-form__header">
          <h2 class="box-title create-form__header-title"> {{ __('New work') }} </h2>
          <div class="display_flex create-form__header-actions">
            <a href="{{ route('user-work-create') }}" >{{ __('forms.create-more') }}</a>
            <a href="{{ route('user-works') }}" class=" ml-15 ">{{ __('forms.list') }}</a>

            <?php
            if ($work) {
              echo '<div>Slug (replace with permalink): '.$work->slug.'</div>';
              echo '<div>Status: '.$work->status->title.'</div>';
              echo Form::start(route('user-work-remove', ['work' => $work]), 'ml-15');
              echo Form::submit('<i class="fa fa-times"></i> ' . __('forms.remove'), 'btn btn-danger btn-block');
              echo Form::end();
            }
            ?>
          </div>
        </div>
        <!-- /.box-header -->
        <?php
        echo Form::start($route, 'box-body');
        echo Form::input('title', __('forms.title'), $work);
        echo Form::textarea('description', __('forms.description'), $work);
        echo Form::input('work_url', __('forms.attached'), $work);
        echo Form::input('thumbnail_url', __('forms.thumbnail'), $work);
        echo Form::select('status_id', __('forms.status'), $statuses, $work ? $work->status_id : old('status_id'));
        echo '<div class="form-group">';
        echo Form::submit('<i class="fa fa-check"></i> ' . __('forms.save'), 'btn btn-success pull-right');
        echo '</div>';
        echo Form::end();
        ?>
        <!-- /.box-body -->


        <!-- /.box-footer -->
    </div>


@endsection
