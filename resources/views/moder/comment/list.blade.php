<?php
use App\Utils\Form;
?>

@extends('layouts.main')

@section('js-settings')
window.app.listAnimator = true;
@endsection

@section('content')
    <div >

        <ul >

            @foreach($comments as $comment)
                <li class="item">
                  <div>
                    <a href="{{ route('moder-work-edit', ['work' => $comment->work]) }}">{{ $comment->work->title }}</a>
                  </div>
                  <div>
                    <span>{{ $comment->user->name }}</span>
                  </div>
                  <p>
                    {!! $comment->text !!}
                  </p>
                  <div>
                    <div>
                      <?php
                      echo Form::start(route('moder-comment-update', ['comment' => $comment]), 'ml-15');
                      echo Form::select('status_id', __('forms.status'), $statuses, $comment->status_id);
                      echo Form::submit('<i class="fa fa-times"></i> ' . __('forms.save'), 'btn btn-success btn-block');
                      echo Form::end();
                      ?>
                    </div>
                    <div>
                      <?php
                      echo Form::start(route('moder-comment-remove', ['comment' => $comment]), 'ml-15');
                      echo Form::submit('<i class="fa fa-times"></i> ' . __('forms.remove'), 'btn btn-danger btn-block');
                      echo Form::end();
                      ?>
                    </div>
                  </div>
                </li>
            @endforeach

          </ul>

          {{ $comments->links() }}

    </div>
@endsection
