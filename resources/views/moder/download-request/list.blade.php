@extends('layouts.main')

<?php

use App\Utils\Form;

?>

@section('js-settings')
window.app.listAnimator = true;
@endsection

@section('content')

<section class="page bg-color_light">
      <div class="row section-head mb-35">
         <div class="col full">
            <h1>@lang('nav.download-requests')</h1>
         </div>
      </div>

      <!-- Blog Entries -->
      <div class="blog-entries row list">

        @foreach($requests as $request)
            <!-- Entry -->
         <article class="entry width_full list-item">

            <div class=" entry-header display_flex width_full pw-10 justify-content_space-between align-items_center">

               <div class="entry-title">
                  <h4><a href="{{ route('public-works-user', ['user' => $request->user]) }}">{{ $request->user->name }}</a></h4> to <a href="{{ route('public-work-show', ['slug' => $request->work->slug]) }}">{{ $request->work->title }}</a>
               </div>

               <div class=" ">
                  <div class="post-meta display_flex ">

                    <?php
                    // Allow form
                    echo Form::start(route('moder-download-request-update', ['request' => $request]), 'mr-15 mb-0');
                    echo Form::submit('<span class="hint__text">'.__('forms.allow').'</span>', 'm-0 width_full button-small button-success button-icon fas fa-check hint__holder');
                    echo Form::end();

                    // Deny form
                    echo Form::start(route('moder-download-request-remove', ['request' => $request]), 'mb-0', 'POST', [
                      'data-modifer' => 'confirmator',
                    ]);
                    echo Form::submit('<span class="hint__text">'.__('forms.deny').'</span>', 'm-0 width_full button-small button-danger button-icon fas fa-ban hint__holder');
                    echo Form::end();
                    ?>

                  </div>
               </div>

            </div>

         </article> <!-- Entry End -->

        @endforeach

      </div> <!-- Blog Entries End -->

      {{ $requests->links() }}

    </section>


@endsection
