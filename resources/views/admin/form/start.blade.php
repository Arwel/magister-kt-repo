<?php
$attsStr = '';
foreach ($atts as $key => $value) {
    $attsStr .= ' '.$key.'="'.$value.'" ';
}

if (!empty($atts['data-form-type']) && $atts['data-form-type'] == 'ajax') {
    ?>
    <script>
        window.app.ajaxForm = true;
    </script>
    <?php
}
?>

<form action="{{ $action }}" method="{{ $method }}" class="{{ $class }}" {!! $attsStr !!}>
    {{ csrf_field() }}