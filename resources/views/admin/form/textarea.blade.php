<div class="{{ $wrapperClass }}">
  <h4>{!! $label !!}</h4>
  <textarea
    class="{{ $class }}"
    name="{{ $name }}"
    rows="{{ $rows }}"
    value="">{!! $value !!}</textarea>
</div>
