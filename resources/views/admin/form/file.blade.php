<script>
    window.app.fileInput = true;
</script>
<div class="form-input_file {{ $wrapperClass }}" data-component="file-input">
    <h4>{!! $label !!}</h4>
    <label class="file-input-label" >
        <span class="btn button-default button-small fas fa-upload button-icon hint__holder">
            <span class="hint__text">@lang('forms.upload')</span>
        </span>
        <input type="file" name="{{ $name }}" data-role="input">
    </label>
    <span class="file-input-name" data-role="file-name"></span>
</div>