<div class="{{ $wrapperClass }}">
  @if($label)
    <h4>{!! $label !!}</h4>
  @endif
  <input
    placeholder="{{ $placeholder }}"
    class="{{ $class }}"
    type="{{ $type }}"
    name="{{ $name }}"
    value="{{ $value }}">
</div>