<div class="{{ $wrapperClass }}">
  <h4>{!! $label !!}</h4>
  <select
    class="{{ $class }}"
    name="{{ $multiple ? $name . '[]' : $name }}"
    {{ $multiple ? 'multiple="multiple"' : '' }}>
    @foreach($options as $value => $option)
      <option
        value="{{ $value }}"
        {{ $option['selected'] ? 'selected="selected"' : '' }}>{{ $option['title'] }}</option>
    @endforeach
  </select>
</div>