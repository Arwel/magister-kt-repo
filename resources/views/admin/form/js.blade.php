<script>
    if (!window.__appConfig) {
        window.__appConfig = {};
    }

    if (!window.__appConfig.components) {
        window.__appConfig.components = [];
    }

    window.__appConfig.components.concat(<?= json_encode($options); ?>);
</script>