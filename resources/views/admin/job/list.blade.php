@extends('layouts.main')

<?php

use App\Utils\Form;

?>

@section('js-settings')
window.app.confirmator = true;
@endsection

@section('content')

<section class="page bg-color_light">
  <div class="row  mb-35">
    <h2>@lang('nav.tasks')</h2>

    <form class="m-0" method="POST" action="{{ route('admin-job-clear') }}" class="pull-right" data-modifer="confirmator">
      {{ csrf_field() }}

      @if ($jobs->isNotEmpty())
      <button type="submit" class="m-0 width_full button-small button-danger"> {{ __('forms.clear') }} </button>
      @endif
    </form>
  </div>

  <!-- Blog Entries -->

  <div class="blog-entries list">

    @foreach($jobs as $job)

    <?php
    $payload = json_decode($job->payload, true);
    $displayName = !empty($payload['displayName'])
        ? $payload['displayName']
        : 'GenericJob';
    ?>
    <!-- Entry -->
    <article class="entry row list-item">

      <div class=" entry-header display_flex justify-content_space-between width_full align-items_center p-15">
        <div class="display_flex flex-direction_col mb-0">
          <div class="dauthor mb-0 pw-15 color_light bg-color_dark font-weight_bold text-align_center">{{ $displayName }} [ {{ (integer)$job->attampts }} ] </div>
        </div>


        <?php
        echo Form::start(route('admin-job-remove', ['job' => $job]), 'mr-0 mb-0 display_flex justify-content_flex-end');
        echo Form::submit('<span class="hint__text">'.__('forms.remove').'</span>', 'button-small button-danger button-icon fas fa-trash hint__holder');
        echo Form::end();
        ?>
      </div>

    </article> <!-- Entry End -->

    @endforeach
  </div> <!-- Blog Entries End -->

  {{ $jobs->links() }}

</section>
@endsection
