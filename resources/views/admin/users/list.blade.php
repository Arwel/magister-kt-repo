@extends('layouts.main')

<?php
use App\Utils\Form;
?>

@section('js-settings')
window.app.confirmator = true;
@endsection

@section('content')

  <section class="page bg-color_light">
      <div class="row  mb-35">
        <h2>@lang('content.users')</h2>
      </div>

      <!-- Blog Entries -->
      <div class="blog-entries row list">

        @foreach($users as $user)
            <!-- Entry -->
         <article class="entry width_full list-item">
            <div class="author-image " style="background-image: url('{{ $user->getAvatar() }}');"></div>

            <div class=" list-item-content display_flex justify-content_space-between width_full align-items_center">


               <div class="entry-title">
                  <h3><a href="{{ route('admin-user-edit', ['user' => $user]) }}">{{ $user->name }}</a></h3>
               </div>

               <div class="">
                  <div class="post-meta">
                    <div class="dauthor mb-15 color_light bg-color_dark font-weight_bold text-align_center pw-10">{{ $user->role->title }}</div>

                    <?php
                    echo Form::start(route('admin-user-remove', ['user' => $user]), 'display_flex justify-content_flex-end', 'POST', [
                      'data-modifer' => 'confirmator',
                    ]);
                    echo Form::submit('<span class="hint__text">'.__('forms.remove').'</span>', 'button-small button-danger button-icon fas fa-trash hint__holder');
                    echo Form::end();
                    ?>


                  </div>
               </div>

            </div>

         </article> <!-- Entry End -->

        @endforeach
      </div> <!-- Blog Entries End -->

        {{ $users->links() }}

    </section>
@endsection
