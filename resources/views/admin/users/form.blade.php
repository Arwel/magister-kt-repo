@extends('layouts.main')

<?php
use App\Utils\Form;
?>
@section('js-settings')
window.app.confirmator = true;
@endsection

@section('content')


<section class="page bg-color_light user-works-form">
  <div class="row">
    <div class="col">

      <div class="box box-primary create-form mb-35" >

        <div class="box-header with-border create-form__header">
          <h1 class="box-title create-form__header-title mb-35">
            @if($user)
            @lang('forms.user'):&nbsp;{{ $user->name }}
            @else
            {{ __('forms.new-user') }}
            @endif
          </h1>

          <div class="display_flex create-form__header-actions">

            @if($user)
            <a href="{{ route('admin-user-create') }}" class=" mr-15 ">{{ __('forms.create-more') }}</a>
            @endif

            <a href="{{ route('admin-users') }}" >{{ __('forms.list') }}</a>
          </div>
          <?php
          if ($user) {
            echo Form::start(route('admin-user-remove', ['user' => $user]), '', 'POST', [
              'data-modifer' => 'confirmator',
            ]);
            echo Form::submit('<span class="hint__text">'.__('forms.remove').'</span>', 'button-small button-danger button-icon fas fa-trash hint__holder');
            echo Form::end();
          }
          ?>
        </div>
        <!-- /.box-header -->
        <?php
        echo Form::start($route, 'box-body', 'POST', [
          'enctype' => 'multipart/form-data',
        ]);

        if ($user) {
          echo '<div class="display_flex justify-content_center"><img src="'.$user->getAvatar().'" class="max-width_150"></div>';
        }

        echo Form::file('avatar', __('forms.avatar'), $user);

        echo Form::input('name', __('forms.name'), $user);


        echo Form::input('email', __('forms.email'), $user, 'email');
        echo Form::input('password', __('forms.password'), $user, false, 'password');
        echo Form::input('password_confirmation', __('forms.password-confirm'), $user, false, 'password');
        echo Form::select('role_id', __('forms.role'), $options, $user ? $user->role_id : old('role_id'), true);
        echo '<div class="form-group">';
        echo Form::submit(__('forms.save'), 'btn btn-success pull-right');
        echo '</div>';
        echo Form::end();
        ?>
        <!-- /.box-body -->


        <!-- /.box-footer -->
      </div>




    </div>
  </div>

  @if ($user)
  <div class="blog-entries list">

    @foreach($user->downloadRequests as $request)
    <!-- Entry -->
    <article class="entry row list-item">

      <div class=" entry-header display_flex justify-content_space-between">

       <div class="col g-9 entry-title">
        <h4>{{ $request->user->name }}</h4> to <a href="{{ route('public-work-show', ['slug' => $request->work->slug]) }}">{{ $request->work->title }}</a>
      </div>

      <div class="col ">
        <div class="post-meta display_flex ">

          <form class="m-0 mt-10 mr-15" method="POST" action="{{ route('moder-download-request-update', ['request' => $request]) }}" >
            {{ csrf_field() }}
            <button type="submit" class="button-small button-success"> {{ __('forms.allow') }} </button>
          </form>

          <form class="m-0 mt-10" method="POST" action="{{ route('moder-download-request-remove', ['request' => $request]) }}" >
            {{ csrf_field() }}
            <button type="submit" class="button-small button-danger"> {{ __('forms.remove') }} </button>
          </form>
        </div>
      </div>

    </div>

  </article> <!-- Entry End -->

  @endforeach

</div> <!-- Blog Entries End -->
@endif
</section>

@endsection
