@extends('layouts.main')

<?php

use App\Utils\Form;

?>

@section('js-settings')
window.app.confirmator = true;
@endsection

@section('content')

<section class="page bg-color_light">
  <div class="row  mb-35">
    <h2>@lang('nav.logs')</h2>

    <form class="m-0" method="POST" action="{{ route('admin-log-clear') }}" class="pull-right" data-modifer="confirmator">
      {{ csrf_field() }}

      @if ($logs->isNotEmpty())
      <button type="submit" class="m-0 width_full button-small button-danger"> {{ __('forms.clear') }} </button>
      @endif
    </form>
  </div>

  <!-- Blog Entries -->

  <div class="blog-entries list">

    @foreach($logs as $log)
    <!-- Entry -->
    <article class="entry row list-item">

      <div class=" entry-header display_flex justify-content_stretch align-items_center">
        <div class="display_flex flex-direction_col ">
          <div class="dauthor mb-15 pw-15 color_light bg-color_dark font-weight_bold text-align_center">{{ $log->tag }}</div>
          <div class="dauthor pw-15 color_light bg-color_dark font-weight_bold text-align_center">{{ $log->getCreatedDateFormatted() }}</div>
        </div>


        <div class="width_flex pw-15 flex-direction_col">
          <h6>
            <a href="#">{{ $log->user->name }}</a>
          </h6>

          <p>
            {{ $log->description }}
          </p>
        </div>

        <?php
        echo Form::start(route('admin-log-remove', ['log' => $log]), 'mr-15 display_flex justify-content_flex-end');
        echo Form::submit('<span class="hint__text">'.__('forms.remove').'</span>', 'button-small button-danger button-icon fas fa-trash hint__holder');
        echo Form::end();
        ?>
      </div>

    </article> <!-- Entry End -->

    @endforeach
  </div> <!-- Blog Entries End -->

  {{ $logs->links() }}

</section>
@endsection
