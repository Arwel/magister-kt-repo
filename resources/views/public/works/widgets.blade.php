<?php
use App\Status;
use App\Utils\Form;

$route = request()->route();
?>

<script>
    window['kt'] = <?= json_encode($diagrams); ?>;
</script>


<div class="widget sort-widget">
    <div class="widget__title">@lang('forms.sort')</div>
    <div class="widget__content">
        <?php
        $sortField = request('field');
        $sortOrder = request('order');

        echo Form::start(route($route->getName(), $route->parameters()), 'display_flex mb-0', 'POST');
        ?>
        <select name="field" class="mb-0 mr-15">
            <option value="created_at" <?= $sortField == 'created_at' ? ' selected="selected" ' : '';  ?>>@lang('forms.date-created')</option>
            <option value="updated_at" <?= $sortField == 'updated_at' ? ' selected="selected" ' : '';  ?>>@lang('forms.date-updated')</option>
            <option value="title" <?= $sortField == 'title' ? ' selected="selected" ' : '';  ?>>@lang('forms.title')</option>
            <option value="plagiarism_percent" <?= $sortField == 'plagiarism_percent' ? ' selected="selected" ' : '';  ?>>@lang('forms.plag-prs')</option>
        </select>

        <select name="order" class="mb-0 mr-15">
            <option value="desc" <?= $sortOrder == 'desc' ? ' selected="selected" ' : '';  ?>>@lang('forms.desc')</option>
            <option value="asc" <?= $sortOrder == 'asc' ? ' selected="selected" ' : '';  ?>>@lang('forms.asc')</option>
        </select>

        <?php
        echo Form::submit('', 'mb-0 fas fa-check');
        echo Form::end();
        ?>
    </div>
</div>

<div class="widget author-diaagram-widget">
    <div class="widget__title">@lang('forms.works-by-author')</div>
    <div class="widget__content">
        <canvas id="works-by-author" data-component="diagram" data-key="author"></canvas>
    </div>
</div>

<div class="widget author-diaagram-widget">
    <div class="widget__title">@lang('forms.works-by-year')</div>
    <div class="widget__content">
        <canvas id="works-by-year" data-component="diagram" data-key="year"></canvas>
    </div>
</div>

<div class="widget author-diaagram-widget">
    <div class="widget__title">@lang('forms.works-by-plagiarism')</div>
    <div class="widget__content">
        <canvas id="works-by-plagiarism" data-component="diagram" data-key="plagiarism"></canvas>
    </div>
</div>

<?php
ob_start();
foreach ($taxonomies as $taxonomy) {
    if ($taxonomy->terms->isEmpty()) {
        continue;
    }

    ?>
    <div class="widget">
        <div class="widget__title">{{ $taxonomy->title }}</div>
        <div class="widget__content">
            <ul>
                <?php
                foreach ($taxonomy->terms as $term) {
                    ?>
                    <li><a href="{{ route('public-works-term', ['term' => $term]) }}">{{ $term->title }}</a></li>
                    <?php
                }
                ?>
            </ul>
        </div>
    </div>
    <?php
}

if (!$yearWorks->isEmpty()) {
    ?>
    <div class="widget">
        <div class="widget__title">@lang('content.works-by-year')</div>
        <div class="widget__content">
            <ul>
                <?php
                foreach ($yearWorks as $work) {
                    ?>
                    <li><a href="{{ route('public-works-year', ['year' => $work->year]) }}">{{ $work->year }}</a></li>
                    <?php
                }
                ?>
            </ul>
        </div>
    </div>
    <?php
}

if (!$authorWorks->isEmpty()) {
    ?>
    <div class="widget">
        <div class="widget__title">@lang('content.authors')</div>
        <div class="widget__content">
            <ul>
                <?php
                foreach ($authorWorks as $authorWork) {
                    if (!$authorWork->user) {
                        continue;
                    }
                    ?>
                    <li><a href="{{ route('public-works-user', ['user' => $authorWork->user]) }}">{{ $authorWork->user->name }}</a></li>
                    <?php
                }
                ?>
            </ul>
        </div>
    </div>
    <?php
}


echo ob_get_clean();
