@extends('layouts.main')

<?php
use App\Utils\Helpers;
?>

@section('js-settings')
window.app.listAnimator = true;
@endsection

@section('content')

<section class="page bg-color_light repository-page">

      @if($title)
        <div class=" section-head mb-35">
           <div class=" full">
              <h1 class="text-align_center">{{ $title }}</h1>
           </div>
        </div>
      @endif

      <div class="repo-wrapper">
        <main>
           <!-- Blog Entries -->
          <div class="blog-entries list">

            @if ($works->isEmpty())
            <div class=" section-head mb-35">
               <div class=" full">
                  <h1 class="text-align_center">@lang('content.empty')</h1>
               </div>
            </div>
            @endif

            @foreach($works as $work)
                <!-- Entry -->
             <article class="entry width_full list-item p-0">
              <div class="author-image " style="background-image: url('{{ $work->getThumbnailUrl() }}');"></div>

              <div class="list-item-content width_full display_flex">
                <div class=" g-9 entry-title mr-15">
                  <h3><a href="{{ route('public-work-show', ['slug' => $work->slug]) }}">{{ $work->title }}</a></h3>
                  <p>
                    {{ Helpers::cropText($work->description) }}
                  </p>

                  <div class="entry__taxonomies">

                    @foreach($work->getTermsLinks() as $taxonomy => $terms)
                      <div>
                        <h6>{{ $taxonomy }}</h6>
                        <p>
                          {!! implode(', ', $terms) !!}
                        </p>
                      </div>
                    @endforeach

                  </div>
                  <div class="progress" >
                    <div class="progress__bar" style="width: {{ $work->plagiarism_percent }}%; "></div>
                    <div class="progress__text">{{ (float)$work->plagiarism_percent }}%</div>
                  </div>

                </div>



                <div class="post-meta-wrapper min-width_100 g-2">
                  <div class="post-meta post-time display_flex flex-direction_col">
                     <a href="{{ route('public-works-year', ['year' => $work->year]) }}">{{ $work->getCreatedDateFormatted() }}</a>
                     @if($work->user)
                     <a href="{{ route('public-works-user', ['user' => $work->user]) }}" class="color-dark_darken3 font-weight_bold">{{ $work->user->name }}</a>
                     @endif
                  </div>
                </div>
              </div>
            </article> <!-- Entry End -->

            @endforeach
          </div> <!-- Blog Entries End -->
          {{ $works->links() }}

        </main>
        <aside class="widget-list">
          {!! $widgets !!}
        </aside>
      </div>

    </section>

@endsection
