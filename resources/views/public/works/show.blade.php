@extends('layouts.main')

<?php
use App\Role;
use App\Status;
use App\Utils\Form;
use Illuminate\Support\Facades\Auth;

$userId = null;
$isModer = false;
$user = Auth::user();
$downloadRequest = null;
$appUrl = config('app.url').'/';
if ($user) {
  $downloadRequest = $user->getDownloadrequest($work);
  $userId = Auth::user()->id;
  $isModer = Auth::user()->role->can(Role::MODER_PRIORITY);
}

$terms = $work ? $work->getTermsLinks() : [];
$hasPreviews = $work && !empty($work->previews);
?>


@if ($hasPreviews)
@section('js-settings')
window.app.lazySlider = {
baseUrl: '<?= $appUrl; ?>',
slides: <?= $work ? json_encode($work->previews) : []; ?>
};
@endsection
@endif


@section('content')

<div class="post-cover__wrapper">
  <div class="post-cover" style="background-image: url({{ $work->getThumbnailUrl() }});">
  </div>

  <div class="post-cover__content">
    <div class="row">
      <h1 class="col color_light width_full text-align_center">{{ $work->title }}</h1>
    </div>
  </div>
</div>

<!-- Post
 ================================================== -->
 <article class="post pt-35">

  <div class="row">

   <div class="col entry-header cf">

    <div class="post-meta justify-content_flex-start">
      <?php
      if ($user && !$downloadRequest) {
        echo Form::start(route('user-download-request-store', ['work' => $work]), 'mr-15 mb-0');
        echo Form::submit('<span class="hint__text">'.__('forms.send-download-request').'</span>', 'button-small button-outline-dark button-icon fas fa-cloud-download-alt hint__holder');
        echo Form::end();
      } elseif ($user && $downloadRequest->canDownload($work, $user)) {
        echo Form::start(route('user-download-request-download', ['work' => $work]), 'mr-15 mb-0');
        echo Form::submit('<span class="hint__text">'.__('forms.download').'</span>', 'button-small button-accent button-icon fas fa-download hint__holder');
        echo Form::end();
      }

      if ($isModer) {
        echo '<a class="btn button-outline-dark button-icon mb-0 mr-30 color_dark color-hover_light fas fa-pen hint__holder" href="'.route('moder-work-edit', ['work' => $work]).'"><span class="hint__text">'.__('forms.edit').'</span></a>';
      } elseif (!$isModer && ($userId == $work->user->id)) {
        echo '<a class="btn button-outline-dark button-icon mb-0  mr-30 color_dark color-hover_light fas fa-pen hint__holder" href="'.route('user-work-edit', ['work' => $work]).'"><span class="hint__text">'.__('forms.edit').'</span></a>';
      }
      ?>
      <a class=" color_dark color-hover_accent " href="{{ route('public-works-year', ['year' => $work->year]) }}">{{ $work->getCreatedDateFormatted() }}</a>
      @if ($work->user)
      &nbsp;/&nbsp;
      <a href="{{ route('public-works-user', ['user' => $work->user]) }}" class="color_dark color-hover_accent">{{ $work->user->name }}</a>
      @endif
    </div>

  </div>

  <p class="col post-content mt-35">
    {!! $work->description !!}
  </p>


@if ($hasPreviews)
  <div class="col post-content mt-35 display_flex width_full flex-direction_col lazy-slider" data-component="lazy-slider">
    <div class="lazy-slider__controls display_flex align-items_stretch">
      <button data-role="prev-btn" class="display_flex lazy-slider__control  button-default button-small fas fa-chevron-left"></button>
      <input type="text" data-role="page-input" class="display_flex lazy-slider__control mb-0">
      <button data-role="next-btn" class="display_flex lazy-slider__control  button-default button-small fas fa-chevron-right"></button>
    </div>
    <div class="lazy-slider__imgs" data-role="img-wrapper">
    </div>
  </div>
  @endif


  @if (!empty($terms))
  <div class="tags">
    @foreach($work->getTermsLinks() as $taxonomy => $terms)
    <div>
      <h6>{{ $taxonomy }}</h6>
      <p>
        {!! implode(', ', $terms) !!}
      </p>
    </div>
    @endforeach
  </div>
  @endif



</div>


</article> <!-- Post End -->

@endsection
