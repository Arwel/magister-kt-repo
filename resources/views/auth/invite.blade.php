@extends('layouts.main')

@section('content')
    <form method="POST" action="{{ route('invite-store') }}">
        {{ csrf_field() }}

        <input type="hidden" name="name" value="{{ $accessRequest->name }}">
        <input type="hidden" name="email" value="{{ $accessRequest->email }}">
        <input type="hidden" name="token" value="{{ $accessRequest->token }}">


        <div>
            <label>
                @lang('forms.avatar')
                <input type="text" name="avatar" >
            </label>
        </div>

        <div>
            <label>
                @lang('forms.password')
                <input type="password" name="password" >
            </label>
        </div>

        <div>
            <label>
                @lang('forms.password-confirm')
                <input type="password" name="password_confirmation" >
            </label>
        </div>

        <div>
            <button type="submit">
                @lang('forms.send')
            </button>
        </div>
    </form>
@endsection
