@extends('layouts.main')

@section('js-settings')
window.app.scrollHeader = true;
@endsection

@section('content')

<section class="bg-color_light page">
    <form class="row" method="POST" action="{{ route('login') }}">
        <div class="col width_full">
            <h3 class="">{{ __('nav.login') }}</h3>
        {{ csrf_field() }}

        <div class="form-group">
            <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('nav.email') }}</label>

            <div class="col-md-6">
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
            </div>
        </div>

        <div class="form-group ">
            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('nav.pass') }}</label>

            <div class="col-md-6">
                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-6 offset-md-4">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                    <label class="form-check-label" for="remember">
                        {{ __('nav.remember') }}
                    </label>
                </div>
            </div>
        </div>

        <div class="form-group mb-0">
            <div class="col-md-8 offset-md-4 display_flex justify-content_space-between align-items_center">
                <button type="submit" class="btn btn-primary m-0">
                    {{ __('nav.login') }}
                </button>

                <a class="btn btn-link" href="{{ route('password.request') }}">
                    {{ __('nav.forgot') }}
                </a>
            </div>
        </div>
        </div>
    </form>
</section>
@endsection
