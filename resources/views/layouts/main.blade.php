<?php
use App\Utils\Form;
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>@lang('layout.title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    {{-- template styles --}}
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('assets/css/lib/base.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/lib/layout.css') }}">


    {{-- /template styles --}}

    @yield('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/main.css') }}">

    <script>window.app = {};</script>
</head>
<body data-spy="scroll" data-target="#nav-wrap">
    @include('parts.preloader')

    @include('parts.nav.menu')

    @include('parts.notifications')

    {{-- content --}}
    @yield('content')
    {{-- /content --}}

    <div class="arw-scroll-top fas fa-arrow-up" data-component="scroll-top">

    </div>

    <script>
        @yield('js-settings')
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.bundle.min.js"></script>

    <script src='https://www.google.com/recaptcha/api.js?render=6LdO134UAAAAAFY0e02xTCmD6RGTEa1h-UAL9_yB'></script>
    <script>



    </script>
    <script src="{{ asset('assets/js/main.min.js') }}"></script>
    @yield('scripts')
</body>
</html>