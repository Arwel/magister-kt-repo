@extends('layouts.main')

@section('js-settings')
window.app.listAnimator = true;
@endsection

@section('content')


    <section class="page bg-color_light">
      <div class="row  mb-35">
        <h2>@lang('content.request-new-user')</h2>
        <?php
        use App\Utils\Form;
        echo Form::ajaxStart(route('admin-request-access-store'), '');
        ?>
        @include('requestAccess.inputs')
        <?php
        echo Form::end();
        ?>
      </div>

      <!-- Blog Entries -->
      <div class="blog-entries list">

        @foreach($requests as $request)
            <!-- Entry -->
         <article class="entry row list-item">

            <div class=" entry-header width_full pw-10 display_flex justify-content_space-between align-items_center">

                <div class="entry-title">
                    <h6>{{ $request->name }}</h6>
                    <p class="mb-0">{{ $request->email }}</p>
                </div>

                <div class="display_flex justify-content_stretch align-items_center">
                    <?php
                    // Allow form
                    echo Form::start(route('request-access-invite', ['request' => $request]), 'mr-15 mb-0');
                    echo Form::submit('<span class="hint__text">'.__('forms.allow').'</span>', 'm-0 width_full button-small button-success button-icon fas fa-check hint__holder');
                    echo Form::end();

                    // Deny form
                    echo Form::start(route('request-access-remove', ['request' => $request]), 'mb-0', 'POST', [
                      'data-modifer' => 'confirmator',
                    ]);
                    echo Form::submit('<span class="hint__text">'.__('forms.deny').'</span>', 'm-0 width_full button-small button-danger button-icon fas fa-ban hint__holder');
                    echo Form::end();
                    ?>
                </div>
            </div>

         </article> <!-- Entry End -->

        @endforeach

      </div> <!-- Blog Entries End -->


      {{ $requests->links() }}


    </section>
@endsection