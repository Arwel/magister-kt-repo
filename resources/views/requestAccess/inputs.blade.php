{{ csrf_field() }}

<div>
    <label>
        @lang('forms.name')
        <input type="text" name="name" value="{{ old('name') }}">
    </label>
</div>

<div>
    <label>
        @lang('forms.email')
        <input type="email" name="email" value="{{ old('email') }}">
    </label>
</div>

<div>
    <button type="submit">
        @lang('forms.send')
    </button>
</div>