@extends('layouts.main')

@section('content')
    <section class="bg-color_light page">
        <?php
        use App\Utils\Form;
        echo Form::ajaxStart(route('request-access-store'), 'row');
        ?>
        <div class="col width_full">
            @include('requestAccess.inputs')
        </div>
        <?php
        echo Form::end();
        ?>
    </section>
@endsection