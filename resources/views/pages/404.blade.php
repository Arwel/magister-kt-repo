@extends('layouts.main')

@section('content')
    <section class="page bg-color_light">
        <div class="row">
            <div class="text-align_center">
                <h1>404</h1>
            </div>
        </div>
    </section>
@endsection
