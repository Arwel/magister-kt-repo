@extends('layouts.main')

@section('js-settings')
window.app.googleMap = true;
@endsection

@section('content')
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css" integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA==" crossorigin=""/>
    <!-- Map Section
     ================================================== -->
     <section class="height_half-view" data-component="google-map">
     </section> <!-- Map Section End-->

    <!-- Contact Section
     ================================================== -->
     <section id="contact-us" class="bg-color_light pt-50 pb-50">

      <div class="row section-head ">
       <div class="col full">
        <h2>@lang('content.contact-us')</h2>
        <p class="desc">@lang('content.contact-us-desc')</p>
      </div>
    </div>

    <div class="row">

     <div class="col g-7">

      <?php
      use App\Utils\Form;

      echo Form::ajaxStart(route('contact-us-send'), 'inputs-width_full ');
      echo Form::input('name', __('forms.name'), null);
      echo Form::input('email', __('forms.email'), null, false, 'email');
      echo Form::textarea('text', __('forms.text'), null);
      echo '<div class="display_flex justify-content_center">';
      echo Form::submit(__('forms.post'), 'button button-wide');
      echo '</div>';
      echo Form::end();
      ?>


 </div>


 <aside class="col g-5">

  <h3>@lang('content.contact-info')</h3>

  <p>
    @lang('content.contact-desc')
  </p>

  <p>
    @lang('content.addr')
  </p>


</aside>

</div>

</section> <!-- Contact Section End-->
@endsection