@extends('layouts.main')

@section('content')
    <!-- About Section
   ================================================== -->
   <section id="about">

      <div class="row section-head">

         <div class="col">
            <h2>@lang('about-page.about-repo')</h2>
         </div>

         <div class="col">
             @lang('about-page.about-repo-text')
         </div>

      </div>

   </section> <!-- Call-to-Action Section End-->
@endsection