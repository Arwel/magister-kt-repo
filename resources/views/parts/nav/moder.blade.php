<?php
use App\Role;
use App\Utils\Menu;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

$user = Auth::user();
?>

<li class="<?= Menu::active('moder-', false); ?> has-sub-menu" data-role="sub-menu">
    <a href="#" data-role="sub-menu-link">@lang('nav.moderate')</a>

    <ul class="sub-menu-list" data-role="sub-menu-list">
        <li >
            <a href="#" class="fas fa-arrow-left" data-role="back-link"></a>
        </li>

        <li class="<?= Menu::active('moder-works'); ?>">
            <a href="{{ route('moder-works') }}">@lang('nav.works')</a>
        </li>

        <li class="<?= Menu::active('moder-taxon', false); ?> has-sub-menu"  data-role="sub-menu">
            <a href="#" data-role="sub-menu-link">@lang('nav.taxonomies')</a>
            <ul class="sub-menu-list" data-role="sub-menu-list">
                <li ><a href="#" class="fas fa-arrow-left" data-role="back-link"></a></li>
                <li class="<?= Menu::active('moder-taxonomies'); ?>"><a href="{{ route('moder-taxonomies') }}">@lang('nav.taxonomies')</a></li>
                <li class="<?= Menu::active('moder-taxonomy-create'); ?>"><a href="{{ route('moder-taxonomy-create') }}">@lang('nav.create-taxonomy')</a></li>
            </ul>
        </li>

        <li class="<?= Menu::active('moder-download-requests'); ?>">
            <a href="{{ route('moder-download-requests') }}">@lang('nav.download-requests')</a>
        </li>
    </ul>
</li>


@if (Auth::user()->role->can(Role::ADMIN_PRIORITY))
<li class="<?= Menu::active('admin-', false); ?> has-sub-menu" data-role="sub-menu">
    <a href="#" data-role="sub-menu-link">@lang('nav.admin-panel')</a>
    <ul class="sub-menu-list" data-role="sub-menu-list">
        <li >
            <a href="#" class="fas fa-arrow-left" data-role="back-link"></a>
        </li>
        <li class="<?= Menu::active('admin-request-access-list'); ?>">
            <a href="{{ route('admin-request-access-list') }}">@lang('nav.requests')</a>
        </li>
        <li class="<?= Menu::active('admin-users'); ?>"><a href="{{ route('admin-users') }}">@lang('nav.users')</a></li>
        <li class="<?= Menu::active('admin-logs'); ?>"><a href="{{ route('admin-logs') }}">@lang('nav.logs')</a></li>
        <li class="<?= Menu::active('admin-jobs'); ?>"><a href="{{ route('admin-jobs') }}">@lang('nav.tasks')</a></li>
    </ul>
</li>
@endif
