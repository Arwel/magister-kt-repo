<?php
use App\Role;
use App\Utils\Menu;
use Illuminate\Support\Facades\Cache;

$user = Auth::user();
if (!$user) {
    return;
}
$role = $user->role;

?>
@if ($role->can(Role::MODER_PRIORITY))
@include('parts.nav.moder')
@endif
<li  class="<?= Menu::active('user-works'); ?>">
    <a href="{{ route('user-works') }}">@lang('nav.works')</a>
</li>
<li  class="<?= Menu::active('user-work-create'); ?>">
    <a href="{{ route('user-work-create') }}">@lang('nav.create-work')</a>
</li>
