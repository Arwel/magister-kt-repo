<div class="display_flex flex-direction_col align-items_center">
    <div>
        <a href="{{ route('request-access') }}" class="button button-small button-outline">@lang('nav.request')</a>
    </div>
    <div class="mt-10">&mdash;</div>
    <div>
        <a href="{{ route('login') }}" class="text-transform_uppercase accent-link">@lang('nav.login')</a>
    </div>
</div>