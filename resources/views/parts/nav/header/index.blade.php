<header class="mini-header" data-component="dynamic-header">
    <div>
        <a href="{{ route('home') }}">
            <img src="#" alt=".">
        </a>
    </div>
    <div data-role="side-btns">
        <div data-role="search-form">
            <form><input type="" name=""></form>
        </div>
        <div>
            <a href="#">Search</a>
            <div class="side-menu__separator"></div>
            <a href="#" data-role="menu-open-btn">Menu</a>
        </div>
        <aside class="side-menu" data-role="side-menu">
            <a href="#" data-role="menu-close-btn">X</a>
            <nav>
                <ul>
                    <li>
                        <a href="{{ route('home') }}">@lang('nav.home')</a>
                    </li>
                </ul>
            </nav>
        </aside>
    </div>
</header>
