<?php
use App\Utils\Form;
use App\Utils\Menu;
use Illuminate\Support\Facades\App;

$locale = App::getLocale();
?>

<!-- Header
  ================================================== -->
  <header class="nav-header" data-component="menu">
    <div class="menu__logo nav-header__item">
      <a href="{{ route('home') }}"><h5 class="color_light">KT Repo</h5></a>
    </div>

    <div class="menu__buttons nav-header__item">
      <a href="#"
      class="menu__btn fas fa-search"
      data-role="open-search"></a>

      <span>&nbsp;|&nbsp;</span>

      <a href="#"
      class="menu__btn fas fa-bars"
      data-role="open-btn"></a>
    </div>

    <div class="menu-search__wrapper" data-role="search-wrapper">
        <?php
        echo Form::start(route('public-works-search'), 'mb-0 menu-search__form no-close', 'GET');
        echo Form::input('search', '', null, true, 'text', 'form-control no-close', 'form-group no-close', __('forms.search-placeholder'));
        echo Form::submit('', 'no-close fa fa-search no-close');
        echo Form::end();
        ?>
    </div>


    <div class="menu-wrapper" data-role="menu">


      <div class="menu-content">
        <div class="menu-content__header">
          <div class="arw-lang-selector" data-component="lang-selector">
            <?php
            echo Form::start(route('home'), 'display_none');
            echo Form::hidden('locale', $locale);
            echo Form::end();
            ?>
            <div class="text-transform_uppercase arw-lang-selector__current arw-lang-selector__item text-transform_uppercase" data-role="current-lang">
              {{ $locale }}
            </div>
            <div class="arw-lang-selector__list" data-role="lang-list">
              @foreach (config('app.supportedLanguages') as $lang)
              <div class="arw-lang-selector__item text-transform_uppercase" data-lang="{{ $lang }}">{{ $lang }}</div>
              @endforeach
            </div>
          </div>

          <a href="#"
          class="fas fa-times menu__btn menu__btn-close"
          data-role="close-btn"></a>
        </div>

        <div class="menu-user-content mb-25">
          @guest
          @include('parts.nav.guest-area')
          @else
          @include('parts.nav.user-area')
          @endguest
        </div>

        <div class="menu-list-wrapper" data-role="menu-list">
          <ul class="menu-list" >
            <li class="<?= Menu::active('home'); ?>"><a href="{{ route('home') }}">@lang('nav.home')</a></li>
            <li class="<?= Menu::active('about'); ?>"><a href="{{ route('about') }}">@lang('nav.about')</a></li>
            <li class="<?= Menu::active('contacts'); ?>"><a href="{{ route('contacts') }}">@lang('nav.contacts')</a></li>

            @guest
            @else
            @include('parts.nav.user')
            @endguest
          </ul>
        </div>

        <div class="menu-content__footer ">
          <div class="menu-content__footer-item copyright ">
            &copy; {{ date('Y') }} &nbsp;|&nbsp;@lang('messages.rights')
          </div>
        </div>
      </div>

    </div>
  </header> <!-- Header End -->
