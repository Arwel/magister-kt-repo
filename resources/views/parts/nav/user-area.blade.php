<?php
use App\Utils\Form;
use Illuminate\Support\Facades\Auth;

$user = Auth::user();
$role = $user->role;

$user->getAvatar();
?>
<div class="menu-user-area">
    <div class="menu-user-area_default">
        <div style="background-image: url('{{ $user->getAvatar() }}');" class="avatar"></div>
    </div>
    <div class="menu-user-area_hover">
        <a class="user-name accent-link" href="{{ route('user-user-edit') }}">
            {{ $user->name }}
        </a>
        <?php
        echo Form::start(route('logout'), 'm-0 pw-15 mt-10 mb-15 width_full');
        echo Form::submit(__('nav.logout'), 'button-small button-outline');
        echo Form::end();
        ?>
    </div>
</div>