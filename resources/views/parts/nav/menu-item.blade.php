<li class="{{ $itemClass }}">
<a href="{{ $route }}">{{ $title }}</a>'
@if ($children)
    {!! view('parts.nav.sub-menu', [
    'items' => $children,
  ]) !!}
@endif

@if ($render)
{!! $render() !!}
@endif
</li>
