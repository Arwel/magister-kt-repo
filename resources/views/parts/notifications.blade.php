<?php
$errorList = $errors->all();
$flashes = session('flash_notification', collect())->toArray();
if (empty($errorList) && empty($flashes)) {
    return;
}
?>

<div class="message-box" data-component="notify-list">
@include('flash::message')

@foreach($errors->all() as $error)
  <div class="alert alert-danger alert-dismissible">
    {{ $error }}
  </div>
@endforeach
</div>