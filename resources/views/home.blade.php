@extends('layouts.main')

@section('js-settings')
  window.app.glideSlider = true;
@endsection

@section('styles')
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/Glide.js/3.2.3/css/glide.core.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/Glide.js/3.2.3/css/glide.theme.min.css">
@endsection

@section('content')
    <!-- Intro Section
   ================================================== -->
   @if (!empty($sliderWorks))
   <section class="glide" data-component="glide-slider">

      <!-- Flexslider Start-->
       <div class="glide__track" data-glide-el="track">

           <ul class="glide__slides">

            @foreach($sliderWorks as $slide)
               <!-- Slide -->
               <li class="glide__slide height_full-view " >
                   <div class=" width_full height_full display_flex justify-content_center align-items_center flex-direction_col has-overlay" style="background-image: url('{{ $slide->getThumbnailUrl()  }}')">
                         <h2 class="color_light">{{ $slide->title }}</h2>
                         <a href="{{ route('public-work-show', ['slug' => $slide->slug]) }}" class="button button-outline mt-35">@lang('home-page.read-more')</a>
                   </div>
               </li>

            <!-- Slide -->
            @endforeach

           </ul>
       </div>
       <!-- Flexslider End-->

       <div class="scroll-down" data-role="scroll-down">
         <svg class="arrow-down">
            <path class="a1" d="M0 0 L15 16 L30 0"></path>
            <path class="a2" d="M0 10 L15 26 L30 10"></path>
            <path class="a3" d="M0 20 L15 36 L30 20"></path>
        </svg>
       </div>

   </section> <!-- Intro Section End-->
   @endif

   <!-- Portfolio Section
   ================================================== -->
   <section id="portfolio">

      <div class="row section-head">
         <div class="col full">

            <h2>@lang('home-page.last-authors')</h2>
            <p class="desc">@lang('home-page.last-authors-desc')</p>

         </div>
      </div>

      <div class="row">

           <!-- Portfolio Wrapper -->
           <div class="authors-wrapper">

            @foreach($authors as $author)
              <a href="{{ route('public-works-user', ['user' => $author]) }}" class="author-item">
                <div class="author-avatar" style="background-image: url('{{ $author->getAvatar() }}');"></div>
                <h5>{{ $author->name }}</h5>
              </a>

            @endforeach

           </div> <!-- Portfolio Wrapper End -->


        </div> <!-- End Row -->


   </section> <!-- Portfolio End -->


   <!-- Services Section
   ================================================== -->
   <section id="services">

      <div class="row section-head">
         <div class="col one-third">
            <h2>@lang('home-page.services')</h2>
         </div>
      </div>

      <div class="row">

         <div class="services-wrapper">

            <div class="col">
               <h2><i class="fas fa-check-double"></i>@lang('home-page.service-fast')</h2>
               <p>@lang('home-page.service-fast-text')</p>
            </div>

            <div class="col">
               <h2><i class="fas fa-briefcase"></i>@lang('home-page.service-easy')</h2>
               <p>@lang('home-page.service-easy-text')</p>
            </div>

            <div class="col m-first">
               <h2><i class="fas fa-shield-alt"></i>@lang('home-page.service-secure')</h2>
               <p>@lang('home-page.service-secure-text')</p>
            </div>



         </div> <!-- Services-Wrapper End -->

      </div> <!--end row -->

   </section> <!-- Services Section End -->



  <section id="cta">
    <div class="row section-head">
       <div class="col full text-align_center">
          <h2>@lang('home-page.want-submit-research')</h2>
          <br>

          @guest
          <a href="{{ route('request-access') }}" class="button button-accent">@lang('home-page.lets-do')</a>
          @else
          <a href="{{ route('user-work-create') }}" class="button button-accent">@lang('home-page.lets-do')</a>
          @endguest

          <br>
          <a href="{{ route('about') }}">@lang('home-page.check-out-rules')</a>
       </div>
    </div>
  </section>





   <!-- Journal Section
   ================================================== -->
   <section id="journal">

      <div class="row section-head">
         <div class="col full">
            <h2>@lang('home-page.last-posts')</h2>
            <p class="desc">@lang('home-page.last-posts-desc')</p>
         </div>
      </div>

      <!-- Blog Entries -->
      <div class="blog-entries">

        @foreach($works as $work)
          <!-- Entry -->
           <article class="entry">

              <div class="row entry-header">

                 <div class="author-image">
                    <img src="{{ $work->getThumbnailUrl() }}" alt="thumb" />
                 </div>

                 <div class="col g-9 offset-1 entry-title">
                    <h3><a href="{{ route('public-work-show', ['slug' => $work->slug]) }}">{{ $work->title }}</a></h3>
                 </div>

                 <div class="col g-2">
                    <p class="post-meta">
                       <time pubdate="" class="post-date" >{{ $work->getCreatedDateFormatted() }}</time>
                       <span class="dauthor">{{ $work->user->name }}</span>
                    </p>
                 </div>

              </div>

              <div class="row">

                 <div class="col g-9 offset-1 post-content">
                    <p>{!! $work->description !!}</p>
                 </div>

              </div>

           </article> <!-- Entry End -->
        @endforeach




      </div> <!-- Blog Entries End -->

   </section>  <!-- Journal Section End -->





@endsection
