@extends('layouts.main')

<?php
use App\Utils\Form;
?>

@section('content')
<div class="post-cover__wrapper">
    <div class="post-cover" style="background-image: url({{ $user->getAvatar() }});">
    </div>

    <div class="post-cover__content">
      <div class="row">
        <h1 class="col color_light width_full text-align_center">{{ $user->name }}</h1>
      </div>
    </div>
  </div>

<section class="page bg-color_light user-works-form">
      <div class="row">
        <div class="col">

        <div class="box box-primary create-form mb-35" >

        <!-- /.box-header -->
        <?php
        echo Form::start($route, 'box-body', 'POST', [
            'enctype' => 'multipart/form-data',
        ]);
        echo Form::input('name', __('forms.name'), $user);
        echo Form::file('avatar', __('forms.avatar'), $user);
        echo Form::input('password', __('forms.password'), $user, false, 'password');
        echo Form::input('password_confirmation', __('forms.password-confirm'), $user, false, 'password');
        echo '<div class="form-group">';
        echo Form::submit(__('forms.save'), 'btn btn-success pull-right');
        echo '</div>';
        echo Form::end();
        ?>
        <!-- /.box-body -->


        <!-- /.box-footer -->
    </div>


        </div>
      </div>
    </section>

@endsection
