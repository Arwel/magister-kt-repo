@extends('layouts.main')

<?php
use App\Utils\Helpers;
?>

@section('js-settings')
window.app.listAnimator = true;
@endsection

@section('content')
    <section class="page bg-color_light ">
      <div class="row section-head mb-35">
         <div class="col full">
            <h1>@lang('content.my-works')</h1>
         </div>
      </div>

      <!-- Blog Entries -->
      <div class="blog-entries row list">

        @foreach($works as $work)
            <!-- Entry -->
         <article class="entry width_full list-item p-0">
            <div class="author-image " style="background-image: url('{{ $work->getThumbnailUrl() }}');"></div>
            <div class="list-item-content display_flex justify-content_space-between width_full align-items_center">
              <div class=" entry-title">
                 <h3><a href="{{ route('user-work-edit', ['work' => $work]) }}">{{ $work->title }}</a></h3>
                 <p>
                   {{ Helpers::cropText($work->description) }}
                 </p>
              </div>

              <div class=" ">
                 <div class="post-meta">
                    <time pubdate="" class="post-date" datetime="{{ $work->getCreatedDateFormatted() }}">{{ $work->getCreatedDateFormatted() }}</time>
                    <div class="dauthor color_light bg-color_dark font-weight_bold text-align_center">{{ $work->status->title }}</div>
                 </div>
              </div>
            </div>


         </article> <!-- Entry End -->

        @endforeach


      </div> <!-- Blog Entries End -->

      {{ $works->links() }}


    </section>
@endsection
