@extends('layouts.main')

<?php
use App\Role;
use App\Utils\Form;
use Illuminate\Support\Facades\Auth;
?>

@section('js-settings')
window.app.confirmator = true;
@endsection

@section('content')
<section class="page bg-color_light user-works-form">
  <div class="row">
    <div class="col">

      <div class="box box-primary create-form mb-35" >

        <div class="box-header with-border create-form__header">
          <h1 class="box-title create-form__header-title mb-35"> {{ __('forms.work') }}
            @if($work)
            <span class="color_light bg-color_dark font-weight_bold pw-15">{{ $work->status->title }}</span>
            @endif
          </h1>

          <div class="display_flex create-form__header-actions">

            @if($work)
            <a href="{{ route('public-work-show', ['slug' => $work->slug]) }}" class="mr-15">@lang('forms.show')</a>
            <a href="{{ route('user-work-create') }}" class=" mr-15">{{ __('forms.create-more') }}</a>
            @endif

            <a href="{{ route('user-works') }}" >{{ __('forms.list') }}</a>
          </div>

          <?php
          if ($work) {
            echo Form::start(route('user-work-remove', ['work' => $work]), 'm-0', 'POST', [
              'data-modifer' => 'confirmator',
            ]);
            echo Form::submit('', 'button-small button-danger button-icon fas fa-trash');
            echo Form::end();
          }
          ?>
        </div>
        <!-- /.box-header -->
        <?php
        echo Form::start($route, 'box-body mt-35', 'POST', [
          'enctype' => 'multipart/form-data',
        ]);
        echo Form::input('title', __('forms.title'), $work);
        echo Form::textarea('description', __('forms.description'), $work);

        echo '<div class="display_flex work-attachments"><div class="work-attachment">';
        // work attachment
        if ($work) {
          echo '<b>'.basename($work->work_url).'</b>';
        }
        echo Form::file('work_url', __('forms.attached'), $work) . '</div>';
        // !work attachment

        // work thumbnail
        echo '<div class="work-attachment__spacer"></div><div class="work-attachment">';
        if ($work) {
          echo '<div class="mt-24"><img src="'.$work->getThumbnailUrl().'" class="max-width_150"></div>';
        }
        echo Form::file('thumbnail_url', __('forms.thumbnail'), $work) . '</div></div>';

        foreach ($taxonomies as $taxonomy) {
          if ($taxonomy->termsLocale->isEmpty()) {
            continue;
          }

          echo '<div>';
          echo Form::select('terms['.$taxonomy->id.']', $taxonomy->title, $taxonomy->termsLocale, $work ? $work->getTermsId() : old('terms['.$taxonomy->id.']'), false, true);
          echo '</div>';
        }

        echo '<div class="form-group">';
        echo Form::submit(__('forms.save'), 'btn btn-success pull-right');
        echo '</div>';
        echo Form::end();
        ?>
        <!-- /.box-body -->


        <!-- /.box-footer -->
      </div>

      <div >

        <?php
        if ($work) {
          $isModer = Auth::user()->role->can(Role::MODER_PRIORITY);

          if ($comments->isNotEmpty()) {
            echo '<h4>'.__('forms.comments').'</h4>';
          }

          foreach ($comments as $comment) {
            $moderForm = '';
            if ($isModer) {
              $moderForm = '
              <div class="display_flex align-items_center">
              <div class="post-meta">
              '.
              Form::start(route('moder-comment-remove', ['comment' => $comment]), 'm-0', 'POST', [
              'data-modifer' => 'confirmator',
              ]).
              Form::submit('', 'button-small button-danger button-icon fas fa-trash').
              Form::end().
              '
              </div>
              </div>
              ';
            }


            echo '
            <!-- Entry -->
            <article class="entry width_full list-item p-0">
            <div class="author-image " style="background-image: url('.$comment->user->getAvatar().');"></div>

            <div class=" list-item-content width_full display_flex  justify-content_space-between">

            <div class="g-9 entry-title">
            <h3>'.$comment->user->name.' <span class="color_light bg-color_dark font-weight_bold pw-15">'.$comment->user->getLabel().'</span></h3>
            <p>
            '.$comment->text.'
            </p>
            </div>

            '.$moderForm.'

            </div>

            </article> <!-- Entry End -->
            ';
          }

          echo Form::start(route('user-comment-store', ['work' => $work, 'status' => 'private']), 'box-body');
          echo Form::textarea('text', __('forms.comment'), null);
          echo '<div class="form-group">';
          echo Form::submit(__('forms.post'), 'btn btn-success pull-right');
          echo '</div>';
          echo Form::end();

          if ($comments->hasMorePages()) {
            echo $comments->links();
          }
        }
        ?>
      </div>

      <div>

        @if ($work && $work->downloadRequests->isNotEmpty())
          <h4>@lang('nav.download-requests')</h4>
          @foreach ($work->downloadRequests as $downloadRequest)

            <article class="entry row list-item">

            <div class=" entry-header display_flex justify-content_space-between">

               <div class="col g-9 entry-title">
                  <h4>{{ $downloadRequest->user->name }}</h4>

               </div>

               <div class="col ">
                  <div class="post-meta display_flex ">

                    <form class="m-0 mt-10 mr-15" method="POST" action="{{ route('user-download-request-update', ['request' => $downloadRequest]) }}" >
                      {{ csrf_field() }}
                      <button type="submit" class="button-small button-success"> {{ __('forms.allow') }} </button>
                    </form>

                    <form class="m-0 mt-10" method="POST" action="{{ route('user-download-request-remove', ['request' => $downloadRequest]) }}" >
                      {{ csrf_field() }}
                      <button type="submit" class="button-small button-danger"> {{ __('forms.remove') }} </button>
                    </form>
                  </div>
               </div>

            </div>

         </article>
          @endforeach



        @endif
      </div>


    </div>
  </div>
</section>

@endsection
