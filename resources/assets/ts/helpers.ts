export function debounce (callback: Function, time: number = 50) {
  let timeout = 0;
  return function () {
    const args = arguments;

    clearTimeout(timeout);
    timeout = setTimeout(() => {
      callback.apply(callback, args);
    }, time);
  }
};

export function tryParseJson (value: string, _default: any = {}): any {
  try {
    return JSON.parse(value);
  } catch (e) {
    return _default;
  }
};

export async function request (url: string, body?: FormData, method: string = 'POST'): Promise<any> {
  return fetch(url, {
    method: method,
    credentials: 'include',
    body
  });
};

export function listenElement (wrapper, path, event, callback) {
  const el = wrapper.querySelector(path);
  el && el.addEventListener(event, callback);
};
