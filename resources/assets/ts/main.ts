/// <reference path="global.d.ts" />


import 'babel-polyfill';
import 'unfetch/polyfill';
import 'intersection-observer';

import mobileMenuManager from './components/nav/mobile-menu';
import notifiesManager from './components/etc/notify';
import langSelector from './components/etc/lang-selctor';
import scrollTop from './components/etc/scroll-top';
import preloaderInit from './components/etc/preloader';
import confirmator from './components/etc/confirmator';
import diagramsInit from './components/etc/diagrams'

import { request } from './helpers';

(async () => {
    preloaderInit();

    if (window.app.fileInput) {
        const func = await import(/* webpackChunkName: "file-input" */ './components/inputs/file');
        func.default();
    }

    if (window.app.googleMap) {
        const func = await import(/* webpackChunkName: "goole-map" */ './components/etc/maps');
        func.default();
    }

    if (window.app.lazySlider) {
        const func = await import(/* webpackChunkName: "lazy-slider" */ './components/etc/lazy-slider/slider');
        func.default();
    }

    if (window.app.ajaxForm) {
        const func = await import(/* webpackChunkName: "ajax-form" */ './components/etc/ajax-form');
        func.default();
    }

    if (window.app.listAnimator) {
        const func = await import(/* webpackChunkName: "ajax-form" */ './components/etc/list-animator');
        func.default();
    }


    mobileMenuManager();
    notifiesManager();
    langSelector();
    scrollTop();
    confirmator();
    diagramsInit();
})();
