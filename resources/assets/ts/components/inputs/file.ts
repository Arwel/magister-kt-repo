class File {
    protected fileName;

    constructor (wrapper) {
        const input = wrapper.querySelector('[data-role="input"]');
        input && input.addEventListener('change', this.inputChangeHandler);

        this.fileName = wrapper.querySelector('[data-role="file-name"]');
    }

    inputChangeHandler = ({target}) => {
        requestAnimationFrame(() => {
            const file = target.files[0];
            this.fileName.innerHTML = file && file.name ? file.name : '...';
        });
    };
}

export default (context = document.body) => {
    for (const input of Array.from(context.querySelectorAll('[data-component="file-input"]'))) {
        new File(input);
    }
};