export default (context = document.body) => {
    const onClickHandler = event => {
        event.preventDefault();
        event.stopPropagation();

        console.log(event.target);

        return false;
    };

    for (const link of Array.from(context.querySelectorAll('a[href="#"]'))) {
        link.addEventListener('click', onClickHandler);
        link.addEventListener('touchstart', onClickHandler);
        link.addEventListener('touchend', onClickHandler);
    }
};
