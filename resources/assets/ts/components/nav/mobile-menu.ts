import {debounce, listenElement} from '../../helpers';

class Menu {
    protected wrapper;
    protected wrapperClasses;
    protected isOpened: boolean;
    protected isScrollState: boolean;
    protected menuList;
    protected lvl;
    protected lastActive;
    protected isSearchOpened;
    protected searchWrapper;
    protected searchInput;

    constructor (wrapper) {
        this.wrapper = wrapper;
        this.wrapperClasses = wrapper.classList;
        this.isOpened = false;
        this.isSearchOpened = false;
        this.isScrollState = false;
        this.lvl = 0;

        // burger menu
        listenElement(wrapper, '[data-role="open-btn"]', 'click', this.toggleClickHandler);
        listenElement(wrapper, '[data-role="close-btn"]', 'click', this.toggleClickHandler);

        // search
        listenElement(wrapper, '[data-role="open-search"]', 'click', this.toggleSearchClickHandler);


        this.menuList = wrapper.querySelector('[data-role="menu-list"]');
        this.searchWrapper = wrapper.querySelector('[data-role="search-wrapper"]');
        this.searchInput = this.searchWrapper.querySelector('input[type="text"]');

        if (window.app.scrollHeader) {
            this.wrapperClasses.add('scroll');
        } else {
            document.addEventListener('scroll', debounce(this.documentScrollHandler));
            this.documentScrollHandler();
        }

        for (const subMenu of Array.from(wrapper.querySelectorAll('[data-role="sub-menu"]'))) {
            this.initSubMenu(subMenu);
        }
    }

    toggleSearchClickHandler = event => {
        event.preventDefault();
        event.stopPropagation();

        if (event.target.classList.contains('no-close')) {
            return;
        }

        if (this.isSearchOpened) {
            this.searchWrapper.classList.remove('active');
            document.removeEventListener('click', this.toggleSearchClickHandler);
        } else {
            this.searchWrapper.classList.add('active');

            document.addEventListener('click', this.toggleSearchClickHandler);
            this.searchInput.focus();
        }

        this.isSearchOpened = !this.isSearchOpened;
    };

    initSubMenu = subMenu => {
        const menuList = subMenu.querySelector('[data-role="sub-menu-list"]');
        requestAnimationFrame(() => {
           menuList && this.menuList.appendChild(menuList);
        });

        listenElement(subMenu, '[data-role="back-link"]', 'click', this.backLinkClickHandler(menuList));
        listenElement(subMenu, '[data-role="sub-menu-link"]', 'click', this.subMenuLinkClickHandler(menuList));
    };

    subMenuLinkClickHandler = wrapper => {

        return event => {
            event.preventDefault();
            event.stopPropagation();

            this.lvl++;
            wrapper.classList.add('active');
            wrapper.style.zIndex = this.lvl;

            this.menuList.classList.add('sub-menu-active');
        };
    };

    backLinkClickHandler = wrapper => {

        return event => {
            event.preventDefault();
            event.stopPropagation();

            this.lvl--;
            wrapper.classList.remove('active');
            wrapper.style.zIndex = '';

            if (this.lvl < 1) {
                this.menuList.classList.remove('sub-menu-active');
            }
        };
    };

    documentScrollHandler = () => {
        if (window.scrollY > 350) {
            this.wrapperClasses.add('scroll');
        } else {
            this.wrapperClasses.remove('scroll');
        }
    };

    toggleClickHandler = event => {
        event.preventDefault();
        event.stopPropagation();

        if (this.isOpened) {
            this.wrapperClasses.remove('active');
        } else {
            this.wrapperClasses.add('active');
        }

        this.isOpened = !this.isOpened;
    };
}

export default (context = document.body) => {
    for (const menu of Array.from(context.querySelectorAll('[data-component="menu"]'))) {
        new Menu(menu);
    }
};
