import { debounce, request, tryParseJson } from '../../helpers';
import Modal from './modal/modal-manager';

class AjaxForm {
    protected form;
    protected method: string;
    protected url: string;
    protected inputs: Map<string, HTMLInputElement>;

    constructor (form) {
        this.form = form;
        this.method = form.method || 'POST';
        this.url = form.action || '/';
        this.inputs = this.inputsMap;

        form.addEventListener('submit', this.formSubmitHandler);
    }

    get inputsMap () {
        const map = new Map<string, HTMLInputElement>()
        for (const input of Array.from<HTMLInputElement>(this.form.querySelectorAll('input[name], textarea[name]'))) {
            map.set(input.name || '', input);
        }

        return map;
    }

    async send () {
        const token = await this.getRecaptchaToken();
        const data = new FormData();
        this.inputs.forEach((input, name) => {
            data.append(name, input.value);
        });
        data.append('token', token as string);

        return await request(this.url, data, this.method);
    }

    formSubmitHandler = async event => {
        event.preventDefault();
        event.stopPropagation();

        this.lock();
        const resp = await this.send();
        let title = '';
        let content = '';

        if (resp.status !== 200) {
            const err = await this.tryParseErrorMsg(resp);

            title = atob(err.title) || 'Title';
            content = atob(err.content) || '';
        } else {
            const json = await resp.json();

            title = atob(json.title) || 'Title';
            content = atob(json.content) || '';
        }

        this.openModal(title, content);
        this.unlock();
    };

    openModal (title, content) {
        try {
            Modal.current
                .setContent(content)
                .setTitle(title)
                .success()
                .open();
        } catch (e) {
            console.error(e);

            this.unlock();
        }
    }

    lock () {
        this.form.classList.add('loading');
    }

    unlock () {
        this.form.classList.remove('loading');
    }

    async tryParseErrorMsg (errResp) {
        const text = await errResp.text();
        const json = tryParseJson(text);

        return {
            title: json.title || 'Error',
            content: json.content || text
        };
    }

    async getRecaptchaToken () {
        return new Promise(resolve => {
            grecaptcha.ready(async () => {
                const token = await grecaptcha.execute('6LdO134UAAAAAFY0e02xTCmD6RGTEa1h-UAL9_yB', {action: 'on_load'});

                resolve(token);
            });
        });
    }
}

export default (context = document.body) => {
    for (const form of Array.from(context.querySelectorAll('[data-form-type="ajax"]'))) {
        new AjaxForm(form)
    }
};
