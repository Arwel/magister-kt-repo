class ListAnimator {
    protected wrapper;
    protected observer;
    protected items;

    constructor (wrapper) {
        this.wrapper = wrapper;
        wrapper.classList.add('list-animated');
        this.observer = new IntersectionObserver(this.itemViewHandler, {
            threshold: .8
        });
        this.items = Array.from(wrapper.children);

        for (const item of this.items) {
            this.observer.observe(item);
        }
    }

    itemViewHandler = items => {
        for (const {target, intersectionRatio} of items) {
            if (intersectionRatio < .8) {
                continue;
            }

            target.classList.add('animated');
            this.observer.unobserve(target);

            const index = this.items.indexOf(target);
            if (index > -1) {
                this.items.splice(index, 1);
            }

            if (this.items.lenght < 1) {
                this.destructor();
            }
        }
    };

    destructor () {
        this.observer.disconnect(this.wrapper);
        this.observer = null;
        this.wrapper = null;
        this.items = null;
    }
}

export default (context = document.body) => {
    for (const list of Array.from(context.querySelectorAll('.list'))) {
        new ListAnimator(list);
    }
};