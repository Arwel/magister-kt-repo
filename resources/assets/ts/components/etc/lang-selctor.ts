class LangSelector {
    protected wrapper;
    protected isActive: boolean;
    protected form;
    protected input;

    constructor (wrapper) {
        this.wrapper = wrapper;
        this.isActive = false;

        const current = wrapper.querySelector('[data-role="current-lang"]');
        current && current.addEventListener('click', this.itemClickHandler(this.toggle));

        const list = wrapper.querySelector('[data-role="lang-list"]');
        list && list.addEventListener('click', this.listClickHandler);

        this.form = wrapper.querySelector('form');
        this.input = this.form.querySelector('input[name="locale"]');
    }

    listClickHandler = ({target}) => {
        if (!target.dataset.lang) {
            return;
        }

        this.input.value = target.dataset.lang;
        this.form.submit();

        this.toggle();
    };

    itemClickHandler = callback => {
        return event => {
            event.preventDefault();
            event.stopPropagation();

            callback();
        };
    };

    toggle = () => {
        if (this.isActive) {
            this.wrapper.classList.remove('active');
            document.removeEventListener('click', this.toggle);
        } else {
            this.wrapper.classList.add('active');
            document.addEventListener('click', this.toggle);
        }

        this.isActive = !this.isActive;
    }
}

export default (context = document.body) => {
    for (const langSelector of Array.from(context.querySelectorAll('[data-component="lang-selector"]'))) {
        new LangSelector(langSelector);
    }
};