
declare const Chart: any;
const singleDiagram = diagram => {
    const config:any = window['kt'][diagram.dataset.key];
    const ctx = diagram.getContext('2d');
    const myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: config.labels,
            datasets: [{
                label: '',
                data: config.data,
                backgroundColor: '#313131',
                borderWidth: 0
            }]
        },
        options: {
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
};



export default () => {
    for (const singleDataDiagram of Array.from(document.querySelectorAll('[data-component="diagram"]'))) {
        singleDiagram(singleDataDiagram);
    }
};
