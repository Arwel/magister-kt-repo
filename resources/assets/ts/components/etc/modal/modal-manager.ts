import { debounce } from '../../../helpers';

export default class Modal {
    protected static inst;

    protected modalTitle;
    protected modalContent;
    protected modalClasses;

    protected constructor () {
        // create modal wrapper
        const wrapper = Modal.createElement('arw-modal');
        this.modalClasses = wrapper.classList;

        // create modal title
        const title = Modal.createElement('arw-modal__title');
        this.modalTitle = Modal.createElement('arw-modal__title-text');
        const closeBtn = Modal.createElement('arw-modal__title-close fa fa-times');
        closeBtn.addEventListener('click', debounce(this.closeClickHandler, 200));

        title.appendChild(this.modalTitle);
        title.appendChild(closeBtn);

        // create modal content
        this.modalContent = Modal.createElement('arw-modal__content');

        // create content wrapper
        const contentWrapper = Modal.createElement('arw-modal__wrapper');

        contentWrapper.appendChild(title);
        contentWrapper.appendChild(this.modalContent);
        wrapper.appendChild(contentWrapper);

        requestAnimationFrame(() => {
            document.body.appendChild(wrapper);
        });
    }

    protected static createElement (className) {
        const elem = document.createElement('div');
        elem.className = className;

        return elem;
    }

    static get current () {
        if (!Modal.inst) {
            Modal.inst = new Modal();
        }

        return Modal.inst;
    }

    closeClickHandler = () => {
        this.classes.add('closing');
        setTimeout(() => {

            this.close();
        }, 200)
    };

    setTitle (title: string) {
        requestAnimationFrame(() => {
            this.modalTitle.innerHTML = this.decodeUtf8(title);
        });

        return this;
    }

    setContent (content: string) {
        requestAnimationFrame(() => {
            this.modalContent.innerHTML = this.decodeUtf8(content);
        });

        return this;
    }

    get classes () {
        return this.modalClasses;
    }

    open () {
        this.classes.add('active');

        return this;
    }

    close () {
        this.classes.remove('active');

        this
        .setContent('')
        .setTitle('')
        .classes.remove('closing');

        this.classes.remove('arw-modal_success');
        this.classes.remove('arw-modal_error');

        return this;
    }

    success () {
        this.classes.add('arw-modal_success');

        return this;
    }

    error () {
        this.classes.add('arw-modal_error');

        return this;
    }

    decodeUtf8 (s: string) {
        return decodeURIComponent(escape(s));
    }
}
