export default class LazySliderItem {
    protected img;
    protected isLoaded: boolean;


    constructor (img) {
        this.img = img;
        this.isLoaded = false;
    }

    activate () {
        if (!this.isLoaded) {
            this.img.addEventListener('load', this.imgLoadHandle);

            this.img.src = this.img.dataset.src;

            this.isLoaded = true;
        } else {
            this.img.classList.add('active');
        }

        return this;
    }

    deactivate () {
        this.img.classList.remove('active');

        return this;
    }

    imgLoadHandle = () => {
        this.img.classList.add('active');

        this.img.removeEventListener('load', this.imgLoadHandle);
    };
}