import LazySliderItem from './slider-item';
import {debounce} from '../../../helpers';

class LazySlider {
    protected wrapper;
    protected items: Array<LazySliderItem>;
    protected activeIndex: number;
    protected activeItem: LazySliderItem | null;
    protected pageInput;
    protected prevBtn;
    protected nextBtn;
    protected img;
    protected imageWrapper;

    constructor (wrapper) {
        this.activeIndex = 0;
        this.activeItem = null;
        this.items = new Array<LazySliderItem>();
        if (!this.config.slides || this.config.slides.length < 1) {
            return;
        }

        this.wrapper = wrapper;

        this.prevBtn = wrapper.querySelector('[data-role="prev-btn"]');
        this.prevBtn && this.prevBtn.addEventListener('click', this.prevClickHandle);

        this.nextBtn = wrapper.querySelector('[data-role="next-btn"]');
        this.nextBtn && this.nextBtn.addEventListener('click', this.nextClickHandle);

        this.pageInput = wrapper.querySelector('[data-role="page-input"]');
        this.pageInput && this.pageInput.addEventListener('change', debounce(this.inputChangeHandle, 200));

        this.imageWrapper = wrapper.querySelector('[data-role="img-wrapper"]');

        this.activate(0);
    }

    get length () {
        return this.config.slides.length;
    }

    get config () {
        return window.app.lazySlider;
    }

    getImage = (): HTMLImageElement => {
        if (!this.img) {
            this.img = document.createElement('img');
            this.img.addEventListener('load', this.imageLoadHandler);
            this.img.classList.add('loading');
            this.img.src = '#';

            requestAnimationFrame(() => {
                this.imageWrapper.appendChild(this.img);
            });
        }

        return this.img;
    };

    imageLoadHandler = () => {
        this.getImage().classList.remove('loading');
    };

    activate (index: number) {
        const {baseUrl, slides} = this.config;
        if (!slides[index]) {
            return false;
        }

        this.getImage().classList.add('loading');
        this.getImage().src = `${baseUrl}${slides[index].url}`;
        this.activeIndex = index;

        requestAnimationFrame(() => {
            this.pageInput.placeholder = `${index + 1} / ${this.length}`;
        });

        if (index < 1) {
            this.prevBtn.classList.add('disabled');
        } else {
            this.prevBtn.classList.remove('disabled');
        }

        if (index > this.length - 2) {
            this.nextBtn.classList.add('disabled');
        } else {
            this.nextBtn.classList.remove('disabled');
        }
    }

    prevClickHandle = event => {
        event.preventDefault();
        event.stopPropagation();

        if (this.activeIndex < 1) {
            return false;
        }

        this.activate(--this.activeIndex);
    };

    inputChangeHandle = event => {
        event.preventDefault();
        event.stopPropagation();

        const number = this.pageInput.value;
        requestAnimationFrame(() => {
            this.pageInput.value = '';
        });

        this.activate(number * 1);
    };

    nextClickHandle = () => {
        if (this.activeIndex > this.length) {
            return false;
        }

        this.activate(++this.activeIndex);
    };
}

export default (context = document.body) => {
    for (const slider of Array.from(context.querySelectorAll('[data-component="lazy-slider"]'))) {
        new LazySlider(slider);
    }
};