export default (context = document.body) => {
    const list = context.querySelector('[data-component="notify-list"]');
    if (!list) {
        return;
    }
    for (const notify of Array.from(list.children)) {
        notify.addEventListener('click', () => {
            requestAnimationFrame(() => {
                if (notify.parentNode === list) {

                    list.removeChild(notify);
                }
            });
        });
    }
};
