import L from 'leaflet';

class GoogleMap {
    constructor (wrapper) {
        const config = GoogleMap.config;
        const map = L.map(wrapper, config);

        L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?' +
            'access_token=pk.' +
            'eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.' +
            'rJcFIG214AriISLbB6B5aw', {
            id: 'mapbox.streets'
        }).addTo(map);

        L.marker([
            config.center.lat,
            config.center.lng
        ]).addTo(map);
    }

    static get config () {
        return {
            center: {lat: 50.7271111, lng: 25.3066537},
            zoom: 16.5
        };
    }
}

export default (context = document.body) => {
    for (const map of Array.from(context.querySelectorAll('[data-component="google-map"]'))) {
        new GoogleMap(map);
    }
};