export default () => {
    const preloader = document.querySelector('[data-component="preloader"]');
    if (!preloader) {
        return;
    }

    window.addEventListener('load', () => {
        document.body.classList.add('loaded');
        preloader.classList.add('loaded');

        setTimeout(() => {
            document.body.removeChild(preloader);
        }, 150);
    });
};
