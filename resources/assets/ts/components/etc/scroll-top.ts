import { debounce } from '../../helpers';

export default (context = document.body) => {
    const btn = context.querySelector('[data-component="scroll-top"]');
    if (!btn) {
        return;
    }

    const checkScrollTop = () => {
        if (window.scrollY > 150) {
            btn.classList.add('active');
        } else {
            btn.classList.remove('active');
        }
    };

    checkScrollTop();
    window.addEventListener('scroll', debounce(checkScrollTop, 150));

    btn.addEventListener('click', debounce(() => {
        window.scroll({
          top: 0,
          left: 0,
          behavior: 'smooth'
        });
    }, 150));
};
