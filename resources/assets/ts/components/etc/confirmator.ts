class Confirmator {
    protected readonly form;

    constructor (wrapper) {
        this.form = wrapper
        wrapper.addEventListener('submit', this.formSubmitHandler);
    }

    formSubmitHandler = event => {
        if (confirm('Are you sure?')) {
            return true;
        }

        event.preventDefault();
        event.stopPropagation();
    };
};

export default (context = document.body) => {
    for (const elem of Array.from(context.querySelectorAll('[data-modifer="confirmator"]'))) {
        new Confirmator(elem);
    }
};
