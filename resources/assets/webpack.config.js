'use strict';
const isProd = process.argv.indexOf('-p') > -1;
const {resolve, join} = require('path');
const mode = isProd ? 'production' : 'development';

module.exports = {
  resolve: {
    extensions: ['.ts', '.js']
  },
  entry  : {
    main   : join(__dirname, 'ts', 'main.ts')
  },
  output : {
    path    : resolve(join(__dirname, '..', '..', 'public', 'assets', 'js')),
    chunkFilename: '[name].module.js',
    filename: '[name].min.js',
    publicPath: '/assets/js/'
  },
  mode   : mode,
  watch  : !isProd,
  devtool: isProd ? false : 'inline-source-map',
  module : {
    rules: [
      {
        test: /\.[jt]s$/,
        loader: 'ts-loader'
      }
    ]
  }
};