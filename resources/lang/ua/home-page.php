<?php
return [
    'read-more' => 'Read More',
    'services' => 'Features',
    'services-desc' => 'Services desc',
    'service-fast' => 'Deep Check',
    'service-fast-text' => 'Our repository provides checking for copied texts. In addition to testing by the moderator, the work goes through tight control of the remote service, which minimizes the human factor in checking the work.',
    'service-easy' => 'Easy To Use',
    'service-easy-text' => 'Using our service, you can quickly and easily find the scientific text according to your needs. You do not need to have your own account, just come, look and enjoy it.',
    'service-secure' => 'Secure Info',
    'service-secure-text' => 'Only we have a unique system of protection against copying of your native work. This method makes it impossible to copy the text from the source code, or anywhere.',
    'last-authors' => 'Recent Authors',
    'last-authors-desc' => 'These authors already taken part with us. Let`s read their works!',
    'last-posts' => 'Latest Works',
    'last-posts-desc' => 'Maybe you want to learn these?',
    'want-submit-research' => 'Want to submit your own research?',
    'lets-do' => 'Let`s Do This!',
    'check-out-rules' => 'Have a question?',
];
