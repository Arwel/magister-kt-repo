<?php
return [
    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    'active' => 'Active',
    'pending' => 'Pending',
    'deleted' => 'Deleted',
    'banned' => 'Banned',
    'private' => 'Private',
    'rejected' => 'Rejected',
];
