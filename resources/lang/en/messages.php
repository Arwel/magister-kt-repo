<?php
return [
    'success-create' => ':name created successfuly!',
    'error-create' => ':name create failed!',

    'success-edit' => ':name updated successfuly!',
    'error-edit' => ':name updated failed!',

    'success-delete' => ':name removed successfuly!',
    'error-delete' => ':name remove failed!',

    'success-invite' => 'User invited successfuly!',
    'error-invite' => 'User invite failed!',

    'success-register' => 'User register successfuly!',
    'error-register' => 'User register failed!',

    'undefined-request' => 'Wrong invite',
    'pass-not-match' => 'Passwords does not match',

    'comments-for-users' => 'Only authenticated users can post comments!',

    'rights' => 'All rights reserved',

    'work-without-file' => 'Can not create work without file',

    'request-access-result' => 'Access request result',

    'item' => 'Item',

    'pass-empty' => 'Password can not be empty',
    'pass-confirm-diff' => 'Pasword and confirmations are not match',

    'comment' => 'comment',
    'taxonomy' => 'taxonomy',
    'term' => 'term',
    'work' => 'work',
    'user' => 'user',
    'invite' => 'invite',

    'log' => 'Log item',
    'download-request' => 'Download request',
];
