<?php
return [
    'about-repo' => 'About the Repository',
    'about-repo-text' => '<p>The University Repository was launched to provide a digital collection of the research output of the University. It strongly encourages all University researchers to deposit their research on open access. All citation data is made available on open access and where possible the ‘full text’ of this content is also accessible.
Material in the repository will conform to the Open Archives Initiative Protocol for Metadata Harvesting and is indexed by Google (Scholar) and Summon, the Library\'s web scale discovery service. Data from the Repository also feeds the research output pages on the Research office’s academic staff profiles.Any University of Huddersfield member of staff, researcher or postgraduate research student, can deposit material. (\'Researcher\' may include visiting fellows/professors, who would also be eligible to deposit material).</p>
<p>Content includes:</p><ul class="marked-list">
<li>Papers published in peer-reviewed journals; in edited journals; in edited conference proceedings; in edited working papers series and in published monographs and book chapters</li>
<li>Post graduate theses registered with the research office, unless a sponsoring body has withheld permission for commercial reasons or there are issues of confidentiality</li>
</ul>'
];
