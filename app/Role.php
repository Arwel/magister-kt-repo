<?php
namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    const ADMIN_PRIORITY = 1000;
    const MODER_PRIORITY = 100;
    const USER_PRIORITY = 1;
    const BANNED_PRIORITY = -1;

    protected $fillable = [
        'title',
        'priority',
        'removable',
    ];

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function can($priority)
    {
        return $this->priority >= (int)$priority;
    }
}
