<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Taxonomy extends Model
{
    protected $fillable = [
        'title',
        'slug',
        'lang',
        'description',
    ];

    use Sluggable;

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title',
            ],
        ];
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function terms()
    {
        return $this->hasMany(Term::class);
    }

    public function termsLocale()
    {
        return $this->hasMany(Term::class)->where('lang', App::getLocale());
    }

    public function getTermsId()
    {
        $ids = [];
        foreach ($this->terms as $term) {
            $ids[] = $term->id;
        }

        return $ids;
    }

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($taxonomy) {
            $taxonomy->terms()->delete();
        });
    }
}
