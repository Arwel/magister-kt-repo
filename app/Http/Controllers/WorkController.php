<?php
namespace App\Http\Controllers;

use App\Status;
use App\Taxonomy;
use App\Term;
use App\User;
use App\Work;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class WorkController extends Controller
{
    public function index(Request $request)
    {
        $works = Work::with('status')
        ->with('user')
        ->where('status_id', Status::ACTIVE)
        ->where('lang', App::getLocale());
        $works = $this->performSorting($request, $works)
        ->paginate(config('app.paginateItems'));

        return view('public.works.list', [
            'works' => $works,
            'title' => null,
            'widgets' => $this->renderWidgets(),
        ]);
    }

    public function bySearch(Request $request)
    {
        $search = $request->get('search', '');

        $works = Work::with('status')
        ->with('user')
        ->where('status_id', Status::ACTIVE)
        ->where('lang', App::getLocale())
        ->where('title', 'like', '%'.$search.'%');
        $works = $this->performSorting($request, $works)
        ->paginate(config('app.paginateItems'));

        return view('public.works.list', [
            'works' => $works,
            'title' => __('content.search-query').' '.$search,
            'widgets' => $this->renderWidgets(),
        ]);
    }

    public function byAuthor(User $user)
    {
        $request = request();

        $works = $user->works()
        ->with('status')
        ->with('user')
        ->where('status_id', Status::ACTIVE)
        ->where('lang', App::getLocale());
        $works = $this->performSorting($request, $works)
        ->paginate(config('app.paginateItems'));

        return view('public.works.list', [
            'works' => $works,
            'title' => __('content.author-works').': '.$user->name,
            'widgets' => $this->renderWidgets(),
        ]);
    }

    public function byYear($year)
    {
        $request = request();
        $this->getValidator(['year' => $year])->validate();

        $works = Work::with('status')
        ->with('user')
        ->where('status_id', Status::ACTIVE)
        ->where('lang', App::getLocale())
        ->where('year', $year);
        $works = $this->performSorting($request, $works)
        ->paginate(config('app.paginateItems'));

        return view('public.works.list', [
            'works' => $works,
            'title' => __('content.works-date').': '.$year,
            'widgets' => $this->renderWidgets(),
        ]);
    }

    public function byTerm(Term $term)
    {
        $request = request();
        $works = $term->works()
        ->with('status')
        ->with('user')
        ->where('status_id', Status::ACTIVE)
        ->where('lang', App::getLocale());
        $works = $this->performSorting($request, $works)
        ->paginate(config('app.paginateItems'));

        return view('public.works.list', [
            'works' => $works,
            'title' => __('content.term-works').': '.$term->title,
            'widgets' => $this->renderWidgets(),
        ]);
    }

    public function show($slug)
    {
        $work = Work::where('slug', $slug)
        ->with('previews')
        ->with('user')
        ->first();

        if (!$work) {
            return redirect()->route('error-404');
        }

        $terms = $work
        ->terms()
        ->with('taxonomy')
        ->get();

        $comments = $work
        ->comments()
        ->with('work')
        ->with('user')
        ->where('comment_id', 0)
        ->where('status_id', '<>', Status::PRIVATE)
        ->paginate(config('app.paginateItems'));

        return view('public.works.show', [
            'work' => $work,
            'terms' => $terms,
            'comments' => $comments,
        ]);
    }

    protected function renderWidgets()
    {
        $locale = App::getLocale();

        $taxonomies = Taxonomy::where('lang', $locale)->with('terms')->get();

        $yearWorks = Work::select('year')
        ->where('status_id', Status::ACTIVE)
        ->where('lang', $locale)
        ->groupBy('year')
        ->get();

        $authorWorks = Work::select('user_id')
        ->with('user')
        ->where('status_id', Status::ACTIVE)
        ->where('lang', $locale)
        ->groupBy('user_id')
        ->get();

        $yearWorksDiagram = $this->getYearDiagram();
        $usersWorksDiagram = $this->getAuthorDiagram();
        $plagiarismWorksDiagram = $this->getPlagiatDiagram();
        // get works per year


        // get works per user


        // get works per plagiat


        return view('public.works.widgets', [
            'taxonomies' => $taxonomies,
            'yearWorks' => $yearWorks,
            'authorWorks' => $authorWorks,
            'diagrams' => [
                'year' => $yearWorksDiagram,
                'author' => $usersWorksDiagram,
                'plagiarism' => $plagiarismWorksDiagram,
            ],
        ]);
    }

    protected function performSorting(Request $request, $workQuery)
    {
        $supportedFields = [
            'title',
            'plagiarism_percent',
            'created_at',
            'updated_at',
        ];
        $supportedOrders = [
            'asc',
            'desc',
        ];

        $field = $request->get('field', '');
        $type = $request->get('order', '');

        if (!(in_array($field, $supportedFields) && in_array($type, $supportedOrders))) {
            return $workQuery;
        }

        return $workQuery->orderBy($field, $type);
    }

    protected function getValidator(array $data)
    {
        $rules = $this->getValidatorRules();

        return Validator::make($data, $rules);
    }

    protected function getValidatorRules()
    {
        return [
            'year' => 'required|integer|min:2010|max:'.date('Y'),
        ];
    }

    protected function getAuthorDiagram()
    {
        $cacheKey = 'authors-diagram';
        $usersWorksDiagram = [
            'labels' => [],
            'data' => [],
        ];

        $cached = Cache::get($cacheKey);
        if (empty($cached)) {
            $result = DB::select(DB::raw('SELECT count(*) as data, name as label FROM works
                                         inner join users on
                                         users.id = works.user_id AND
                                         works.status_id = 1 AND
                                         works.lang = "'.App::getLocale().'"
                                         GROUP BY works.user_id
                                         LIMIT 7'
                                     ));

            if (!is_array($result)) {
                $result = [];
            }

            foreach ($result as $row) {
                $usersWorksDiagram['labels'][] = $row->label;
                $usersWorksDiagram['data'][] = $row->data;
            }

            Cache::put($cacheKey, json_encode($usersWorksDiagram));
        } else {
            $usersWorksDiagram = json_decode($cached, true);

            if (!is_array($usersWorksDiagram)) {
                $usersWorksDiagram = [
                    'labels' => [],
                    'data' => [],
                ];
            }
        }

        return $usersWorksDiagram;
    }

    protected function getYearDiagram()
    {
        $cacheKey = 'year-diagram';
        $yearWorksDiagram = [
            'labels' => [],
            'data' => [],
        ];

        $cached = Cache::get($cacheKey);
        if (empty($cached)) {
            $result = DB::select(DB::raw('SELECT count(*) as data, year as label FROM works WHERE
                                         works.status_id = 1 AND
                                         works.lang = "'.App::getLocale().'"
                                         GROUP BY year
                                         LIMIT 7'
                                     ));

            if (!is_array($result)) {
                $result = [];
            }

            foreach ($result as $row) {
                $yearWorksDiagram['labels'][] = $row->label;
                $yearWorksDiagram['data'][] = $row->data;
            }

            Cache::put($cacheKey, json_encode($yearWorksDiagram));
        } else {
            $yearWorksDiagram = json_decode($cached, true);

            if (!is_array($yearWorksDiagram)) {
                $yearWorksDiagram = [
                    'labels' => [],
                    'data' => [],
                ];
            }
        }

        return $yearWorksDiagram;
    }

    protected function getPlagiatDiagram()
    {
        $cacheKey = 'plagiat-diagram';
        $plagiarismWorksDiagram = [
            'labels' => [],
            'data' => [],
        ];

        $cached = Cache::get($cacheKey);
        if (empty($cached)) {
            $result = DB::select(DB::raw('SELECT plagiarism_percent as data, title as label FROM works
                                         WHERE
                                         works.status_id = 1 AND
                                         data > 0 AND
                                         works.lang = "'.App::getLocale().'"
                                         ORDER BY created_at DESC
                                         LIMIT 7'
                                     ));

            if (!is_array($result)) {
                $result = [];
            }

            foreach ($result as $row) {
                $plagiarismWorksDiagram['labels'][] = strlen($row->label) > 10 ? substr($row->label, 0, 10).'...' : $row->label;
                $plagiarismWorksDiagram['data'][] = $row->data;
            }

            Cache::put($cacheKey, json_encode($plagiarismWorksDiagram));
        } else {
            $plagiarismWorksDiagram = json_decode($cached, true);

            if (!is_array($plagiarismWorksDiagram)) {
                $plagiarismWorksDiagram = [
                    'labels' => [],
                    'data' => [],
                ];
            }
        }

        return $plagiarismWorksDiagram;
    }
}
