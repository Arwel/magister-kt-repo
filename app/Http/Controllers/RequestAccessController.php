<?php
namespace App\Http\Controllers;

use App\Abstractions\HasFlashesTrait;
use App\AccessRequest;
use App\AdminLog;
use App\Http\Requests\InviteRequest;
use App\Mail\InviteMail;
use App\Role;
use App\Services\Recaptcha;
use App\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class RequestAccessController extends Controller
{
    use HasFlashesTrait;

    public function index()
    {
        $requests = AccessRequest::paginate(config('app.paginateItems'));

        return view('requestAccess.list', [
            'requests' => $requests,
        ]);
    }

    public function create()
    {
        return view('requestAccess.form');
    }

    public function store(InviteRequest $request, Recaptcha $recaptcha)
    {
        $response = '';
        $validator = null;
        try {
            if ($recaptcha->isBot($request->get('token'))) {
                return $recaptcha->botResponse();
            }

            $data = $request->only(['name', 'email']);
            $validator = $request->getValidator();
            $isFails = $validator->fails();

            if ($isFails) {
                throw new Exception('Data invalid');
            }

            if (User::where('email', $data['email'])->first()) {
                throw new Exception(__('forms.user-exists'));
            }

            $isAdmin = Auth::user() && Auth::user()->role->can(Role::ADMIN_PRIORITY);
            $created = AccessRequest::create([
                'name' => $data['name'],
                'token' => AccessRequest::getNewToken(),
                'email' => $data['email'],
            ]);

            switch (true) {
                case $isAdmin:
                    $response = $this->mail($created);
                    $response = $response['message'];
                    break;
                case (!!$created):
                    $response = __('forms.request-access-success');
                    break;
                default:
                    $response = __('messages.error-create', __('messages.invite'));
                    break;
            }
        } catch (Exception $e) {
            $response = $e->getMessage();
            if ($isFails) {
                $response = implode('<br>', $validator->errors()->all());
            }
        } finally {
            return response([
                'title' => base64_encode(__('messages.request-access-result')),
                'content' => base64_encode($response),
            ], 200);
        }
    }

    public function remove(AccessRequest $request)
    {
        if ($request->delete()) {
            flash(__('messages.success-delete'))->success();
        } else {
            flash(__('messages.error-delete'))->error();
        }

        return redirect()->route('admin-request-access-list');
    }

    public function sendInvite(AccessRequest $request)
    {
        $result = $this->mail($request);
        $flash = flash($result['message']);

        // AdminLog::created('Invite user '.$request->get('email'));

        if ($result['result']) {
            $flash->success();
        } else {
            $flash->error();
        }

        return redirect()->route('admin-request-access-list');
    }

    protected function mail(AccessRequest $request)
    {
        try {
            Mail::to($request->email)->send(new InviteMail($request));
        } catch (Exception $e) {
            return [
                'result' => false,
                'message' => $e->getMessage(),
            ];
        }

        if ($request->delete()) {
            return [
                'result' => true,
                'message' => __('messages.success-invite'),
            ];
        }

        return [
            'result' => false,
            'message' => __('messages.error-invite'),
        ];
    }

    protected function getValidator(array $data)
    {
        $rules = $this->getValidatorRules();

        return Validator::make($data, $rules);
    }

    protected function getValidatorRules()
    {
        return [
            'name' => 'required|string|max:255|unique:access_requests',
            'email' => 'required|string|email|max:255|unique:access_requests',
        ];
    }
}
