<?php
namespace App\Http\Controllers;

use App\Http\Requests\ContactUsRequest;
use App\Mail\ContactUsMail;
use App\Mail\InviteMail;
use App\Services\Recaptcha;
use App\Status;
use App\Work;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class PagesController extends Controller
{
    public function home()
    {
        $authors = [];
        $works = Work::select('user_id')
            ->with('user')
            ->where('status_id', Status::ACTIVE)
            ->groupBy('user_id')
            ->orderBy('created_at', 'desc')
            ->limit(3)
            ->get();
        foreach ($works as $work) {
            $authors[] = $work->user;
        }

        $works = Work::with('user')
            ->limit(3)
            ->where('status_id', Status::ACTIVE)
            ->orderBy('created_at', 'desc')
            ->get();

        $sliderWorks = Work::limit(3)
            ->where('status_id', Status::ACTIVE)
            ->orderBy('created_at', 'desc')
            ->get();


        return view('home', [
            'authors' => $authors,
            'works' => $works,
            'sliderWorks' => $sliderWorks,
        ]);
    }

    public function about()
    {
        return view('pages.about');
    }

    public function contacts()
    {
        return view('pages.contacts');
    }

    public function ajaxContactUs(ContactUsRequest $request, Recaptcha $recaptcha)
    {
        $validator = $request->getValidator();
        $isFailed = $validator->fails();

        if ($recaptcha->isBot($request->get('token'))) {
            return $recaptcha->botResponse();
        }

        $response = '';
        $code = 200;

        try {
            if ($isFailed) {
                throw new Exception('validation failed');
            }

            Mail::to($request->email)->send(new ContactUsMail($request));
            $response = __('forms.contact-us-success');
        } catch (Exception $e) {
            $response = $e->getMessage();
            $code = 400;

            if ($isFailed) {
                $response = implode('<br>', $validator->errors()->all());
            }
        } finally {
            return response([
                'title' => base64_encode(__('forms.contact-us-result')),
                'content' => base64_encode($response),
            ], $code);
        }
    }

    public function error404()
    {
        return view('pages.404');
    }

    public function error403()
    {
        return view('pages.403');
    }
}
