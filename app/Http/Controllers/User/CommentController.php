<?php
namespace App\Http\Controllers\User;

use App\Abstractions\HasFlashesTrait;
use App\Comment;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserCommentRequest;
use App\Status;
use App\Work;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    use HasFlashesTrait;

    public function replay(Work $work, Comment $parent = null)
    {
        return $this->create($work, null, $parent);
    }

    public function storeReplay(Work $work, UserCommentRequest $request, Comment $parent = null)
    {
        return $this->store($work, $request, $parent);
    }

    public function store(Work $work, UserCommentRequest $request, $status = 'pending', Comment $parent = null)
    {

        $comment = Comment::create([
            'text' => $request->input('text'),
            'user_id' => Auth::user()->id,
            'work_id' => $work->id,
            'status_id' => $this->getValidStatus($status),
            'comment_id' => $parent ? $parent->id : 0,
        ]);

        $this->createFlashes($comment, __('messages.comment'));

        return redirect()->back();
    }

    public function update(Work $work, Comment $comment, CommentRequest $request)
    {
        $data = $request->all();
        $this->getValidator($data, false)->validate();
        $values = $request->only($work->getFillable());

        $this->updateFlashes($comment->fill($values)->save(), __('messages.comment'));

        return redirect()->back();
    }

    public function remove(Comment $comment)
    {
        $this->deleteFlashes($comment->delete(), __('messages.comment'));

        return redirect()->back();
    }

    protected function getValidStatus($key)
    {
        switch ($key) {
            case 'private':
                return Status::PRIVATE;
            default:
                return Status::PENDING;
        }
    }
}
