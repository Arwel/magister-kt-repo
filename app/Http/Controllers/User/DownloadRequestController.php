<?php

namespace App\Http\Controllers\User;

use App\Abstractions\HasFlashesTrait;
use App\DownloadRequest;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserDownloadRequest;
use App\Role;
use App\Status;
use App\Utils\Files;
use App\Work;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DownloadRequestController extends Controller
{
    use HasFlashesTrait;

    public function store(Work $work)
    {
        $user = Auth::user();
        $userId = $user->id;

        try {
            if ($user->downloadRequests()->where('work_id', $work->id)->first()) {
                throw new Exception('You have request for this work');
            }

            $download = DownloadRequest::create([
                'user_id' => $userId,
                'work_id' => $work->id,
                'status_id' => Status::PENDING,
            ]);

            $this->createFlashes($download, __('messages.download-request'));
        } catch (Exception $e) {
            flash($e->getMessage())->error();
        } finally {
            return redirect()->back();
        }
    }

    public function download(Work $work)
    {
        $user = Auth::user();
        $downloadRequest = $user->getDownloadRequest($work);

        if ($user->role->can(Role::MODER_PRIORITY) || $downloadRequest->status_id == Status::ACTIVE) {
            $privateDiskName = Files::getPrivateDiskName();
            $disk = Files::getDisk($privateDiskName);
            $path = str_replace($disk->url(''), '', $work->getWorkUrl());

            return $disk->download($path, basename($path));
        }

        return redirect()->back();
    }

    public function remove(DownloadRequest $request)
    {
        if ($request->work->user_id == Auth::user()->id) {
            $this->deleteFlashes($request->delete(), __('messages.download-request'));
        }

        return redirect()->back();
    }

    public function update(DownloadRequest $request)
    {
        if ($request->work->user_id == Auth::user()->id) {
            $request->status_id = Status::ACTIVE;
            $this->updateFlashes($request->save(), __('messages.download-request'));
        }

        return redirect()->back();
    }
}
