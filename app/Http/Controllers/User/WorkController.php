<?php
namespace App\Http\Controllers\User;

use App\Abstractions\HasFlashesTrait;
use App\Abstractions\HasLangValidateTrait;
use App\Http\Controllers\Controller;
use App\Http\Requests\ModerWorkRequest;
use App\Http\Requests\UserWorkRequest;
use App\Role;
use App\Status;
use App\Taxonomy;
use App\Utils\Files;
use App\Work;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class WorkController extends Controller
{
    use HasFlashesTrait;
    use HasLangValidateTrait;

    public function index()
    {
        $works = Auth::user()
            ->works()
            ->with('status')
            ->paginate(config('app.paginateItems'));

        return view('user.work.list', [
            'works' => $works,
        ]);
    }

    public function create($work = null)
    {
        $route = $work ? route('user-work-update', [
            'work' => $work,
        ]) : route('user-work-store');

        $taxonomies = Taxonomy::with('termsLocale')->get();

        $comments = empty($work)
            ? []
            : $work
                ->comments()
                ->with('user')
                ->where('status_id', Status::PRIVATE)
                ->where('comment_id', 0)
                ->paginate(config('app.paginateItems'));

        return view('user.work.form', [
            'work' => $work,
            'route' => $route,
            'taxonomies' => $taxonomies,
            'comments' => $comments,
        ]);
    }

    public function store(UserWorkRequest $request)
    {
        if (!$request->hasFile('work_url')) {
            flash(__('messages.work-without-file'))->error();

            return redirect()->back();
        }

        $data = $request->only(['title', 'description', 'lang']);
        $validated = $request->validated();
        $data['work_url'] = $this->upload($request, 'work_url', true);

        $data['thumbnail_url'] = $request->hasFile('thumbnail_url')
            ? $this->upload($request, 'thumbnail_url')
            : '';

        $work = Work::create([
            'title' => $data['title'],
            'description' => $data['description'],
            'work_url' => $data['work_url'],
            'lang' => App::getLocale(),
            'thumbnail_url' => $data['thumbnail_url'],
            'user_id' => Auth::user()->id,
            'year' => date('Y'),
        ]);

        $work && $work->syncTerms($request->get('terms', []));

        $this->createFlashes($work, __('messages.work'));

        return $work
            ? redirect()->route('user-work-edit', [
                'work' => $work,
            ])
            : $this->create();
    }

    public function edit(Work $work)
    {
        if (!Auth::user()->canEdit($work)) {
            return redirect()->route('error-403');
        }

        return $this->create($work);
    }

    public function update(Work $work, UserWorkRequest $request)
    {
        if (!Auth::user()->canEdit($work)) {
            return redirect()->route('error-403');
        }

        $data = $request->all();
        $values = $request->only($work->getFillable());

        $values['status_id'] = Status::PENDING;

        if ($request->hasFile('work_url')) {
            $values['work_url'] = $this->upload($request, 'work_url', true);
        }

        if ($request->hasFile('thumbnail_url')) {
            $values['thumbnail_url'] = $this->upload($request, 'thumbnail_url');
        }

        $work->syncTerms($request->get('terms', []));

        $this->updateFlashes($work->fill($values)->save(), __('messages.work'));

        return $this->create($work);
    }

    public function remove(Work $work)
    {
        if (!Auth::user()->canEdit($work)) {
            return redirect()->route('error-403');
        }

        $this->deleteFlashes($work->delete(), __('messages.work'));

        return redirect()->route('user-works');
    }

    protected function upload(Request $request, $name, $secure = false)
    {
        $file = $request->file($name);

        return Files::upload($file, null, $secure);
    }
}
