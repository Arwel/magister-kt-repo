<?php
namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use App\Utils\Files;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function edit()
    {
        return view('user.user.form', [
            'user' => Auth::user(),
            'route' => route('user-user-update'),
        ]);
    }

    public function update(Request $request)
    {
        $user = Auth::user();

        $data = $request->all();
        $this->getValidator($data)->validate();
        $passConfirm = $request->get('password_confirmation', '');

        $values = array_intersect_key($data, array_flip($user->getFillable()));
        $values = $this->filterValues($values, $passConfirm);

        if ($request->hasFile('avatar')) {
            $values['avatar'] = $this->uploadAvatar($request);
        }

        if ($user->fill($values)->save()) {
            flash(__('messages.success-edit', ['name' => 'User']))->success();
        } else {
            flash(__('messages.error-edit'))->error();
        }

        return redirect()->back();
    }

    protected function uploadAvatar(Request $request)
    {
        $file = $request->file('avatar');

        return Files::upload($file);
    }

    protected function filterValues(array $values, $passConfirm)
    {
        if (isset($values['email'])) {
            unset($values['email']);
        }

        if (isset($values['role_id'])) {
            unset($values['role_id']);
        }

        if (empty($values['password'])) {
            unset($values['password']);
        } elseif (!empty($values['password']) && !empty($passConfirm) && $passConfirm == $values['password']) {
            $values['password'] = Hash::make($values['password']);
        } elseif (!empty($values['password'])) {
            flash(__('messages.pass-not-match'))->error();
            unset($values['password']);
        }

        return $values;
    }

    protected function getValidator(array $data)
    {
        $rules = $this->getValidatorRules();

        return Validator::make($data, $rules);
    }

    protected function getValidatorRules()
    {
        return [
            'name' => 'required|string|max:255',
            'password' => 'sometimes|nullable|string|min:6|confirmed',
        ];
    }
}
