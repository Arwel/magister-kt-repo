<?php

namespace App\Http\Controllers\Auth;

use App\AccessRequest;
use App\Http\Controllers\Controller;
use App\Jobs\AvatarGenerateJob;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function inviteForm($token)
    {
        $request = AccessRequest::where('token', $token)->where('invited', true)->first();

        if (!($request instanceof AccessRequest)) {
            flash(__('messages.undefined-request'))->error();

            return redirect()->route('home');
        }

        return view('auth.invite', [
            'accessRequest' => $request,
        ]);
    }

    public function inviteStore(Request $request)
    {
        $data = $request->all();
        Validator::make($data, [
            'name' => 'required|string|max:255',
            'avatar' => 'required|string|max:255',
            'token' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        $accessRequest = AccessRequest::where('token', $data['token'])
            ->where('email', $data['email'])
            ->where('invited', true)
            ->where('name', $data['name'])
            ->first();

        if (!($accessRequest instanceof AccessRequest)) {
            flash(__('messages.undefined-request'))->error();

            return redirect()->route('home');
        }

        $accessRequest->delete();

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'avatar' => $data['avatar'],
            'password' => Hash::make($data['password']),
        ]);

        if ($user) {
            Auth::login($user);
            AvatarGenerateJob::dispatch($user)->onQueue('database');
            flash(__('messages.success-register'))->success();
        } else {
            flash(__('messages.error-register'))->error();
        }


        return redirect()->route('home');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }
}
