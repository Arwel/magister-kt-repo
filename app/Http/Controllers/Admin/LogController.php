<?php

namespace App\Http\Controllers\Admin;

use App\Abstractions\HasFlashesTrait;
use App\AdminLog;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LogController extends Controller
{
    use HasFlashesTrait;

    public function index()
    {
        $logs = AdminLog::with('user')
            ->orderBy('created_at', 'desc')
            ->paginate(config('app.paginateItems'));

        return view('admin.log.list', [
            'logs' => $logs,
        ]);
    }

    public function remove(AdminLog $log)
    {
        $this->deleteFlashes($log->delete(), __('messages.log'));

        return redirect()->back();
    }

    public function clear()
    {
        $this->deleteFlashes(AdminLog::truncate(), __('messages.log'));

        return redirect()->back();
    }
}
