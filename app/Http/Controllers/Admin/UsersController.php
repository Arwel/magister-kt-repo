<?php

namespace App\Http\Controllers\Admin;

use App\Abstractions\HasFlashesTrait;
use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use App\Utils\Files;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller
{
    use HasFlashesTrait;

    public function index()
    {
        $users = User::with('role')->paginate(config('app.paginateItems'));

        return view('admin.users.list', [
            'users' => $users,
        ]);
    }

    public function create($user = null)
    {
        $route = $user
            ? route('admin-user-edit', ['user' => $user])
            : route('admin-user-store');

        return view('admin.users.form', [
            'options' => Role::all(),
            'user' => $user,
            'route' => $route,
        ]);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $this->getValidator($data)->validate();

        $data['avatar'] = $request->hasFile('avatar')
            ? $this->uploadAvatar($request)
            : '';

        $pass = $request->input('password');
        $passConfirm = $request->input('password_confirmation');
        if (empty($pass)) {
            flash(__('messages.pass-empty'))->error();

            return redirect()->back();
        }

        if ($pass != $passConfirm) {
            flash(__('messages.pass-confirm-diff'))->error();

            return redirect()->back();
        }

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'role_id' => $data['role_id'],
            'avatar' => $data['avatar'],
        ]);

        $this->createFlashes($user, __('messages.user'));

        return $user
            ? redirect()->route('admin.user.edit', [
                'user' => $user,
            ])
            : $this->create();
    }

    public function edit(User $user)
    {
        return $this->create($user);
    }

    public function update(User $user, Request $request)
    {
        $data = $request->all();
        $this->getValidator($data, true)->validate();
        $passConfirm = $request->get('password_confirmation', '');

        $values = array_intersect_key($data, array_flip($user->getFillable()));

        if ($request->hasFile('avatar')) {
            $values['avatar'] = $this->uploadAvatar($request);
        }

        if (empty($values['password'])) {
            unset($values['password']);
        } elseif (!empty($values['password']) && !empty($passConfirm) && $passConfirm == $values['password']) {
            $values['password'] = Hash::make($values['password']);
        } elseif (!empty($values['password'])) {
            flash(__('messages.pass-not-match'))->error();
            unset($values['password']);
        }

        $this->updateFlashes($user->fill($values)->save(), __('messages.user'));

        return $this->create($user);
    }

    public function remove(User $user)
    {
        $this->deleteFlashes($user->delete(), __('messages.user'));

        return redirect()->route('admin-users');
    }

    protected function uploadAvatar(Request $request)
    {
        $file = $request->file('avatar');

        return Files::upload($file);
    }

    protected function getValidator(array $data, $edit = false)
    {
        $rules = $this->getValidatorRules($edit);

        return Validator::make($data, $rules);
    }

    protected function getValidatorRules($edit = false)
    {
        return $edit ?
        [
            'name' => 'required|string|max:255',
            'role_id' => 'required|integer|min:0',
            'email' => 'required|string|email|max:255',
            'password' => 'sometimes|nullable|string|min:6|confirmed',
        ] :
        [
            'name' => 'required|string|max:255',
            'role_id' => 'required|integer|min:0',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ];
    }
}
