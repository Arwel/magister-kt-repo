<?php

namespace App\Http\Controllers\Admin;

use App\Abstractions\HasFlashesTrait;
use App\AdminLog;
use App\Http\Controllers\Controller;
use App\Job;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class JobController extends Controller
{
    use HasFlashesTrait;

    public function index()
    {
        $jobs = Job::orderBy('created_at', 'desc')
            ->paginate(config('app.paginateItems'));

        return view('admin.job.list', [
            'jobs' => $jobs,
        ]);
    }

    public function remove(Job $job)
    {
        $this->deleteFlashes($job->delete(), __('messages.log'));

        return redirect()->back();
    }

    public function clear()
    {
        $this->deleteFlashes(Job::truncate(), __('messages.log'));

        return redirect()->back();
    }
}
