<?php
namespace App\Http\Controllers\Moder;

use App\Abstractions\HasFlashesTrait;
use App\Abstractions\HasLangValidateTrait;
use App\AdminLog;
use App\Events\OnWorkPreviewGenerated;
use App\Http\Controllers\Controller;
use App\Http\Requests\ModerWorkRequest;
use App\Jobs\CheckWorkPlagiarismJob;
use App\Jobs\GenerateWorkPreviewJob;
use App\Services\Recaptcha;
use App\Status;
use App\Taxonomy;
use App\Utils\Files;
use App\Work;
use ConvertApi\ConvertApi;
use Copyleaks\Config as CopyleaksConfig;
use Copyleaks\CopyleaksCloud;
use Copyleaks\CopyleaksProcess;
use Copyleaks\Products;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class WorkController extends Controller
{
    use HasFlashesTrait;
    use HasLangValidateTrait;

    public function index($filter = 'pending')
    {
        $works = Work::with('status');
        if ($filter != 'all') {
            $works = $works->where('status_id', Status::PENDING);
        }

        return view('moder.work.list', [
            'works' => $works->paginate(config('app.paginateItems')),
        ]);
    }

    public function create($work = null)
    {
        $route = $work ? route('moder-work-update', [
            'work' => $work,
        ]) : route('moder-work-store');

        $comments = empty($work)
            ? []
            : $work
                ->comments()
                ->with('user')
                ->where('status_id', Status::PRIVATE)
                ->where('comment_id', 0)
                ->paginate(config('app.paginateItems'));

        return view('moder.work.form', [
            'statuses' => Status::all(),
            'taxonomies' => Taxonomy::with('termsLocale')->get(),
            'work' => $work,
            'route' => $route,
            'comments' => $comments,
        ]);
    }

    public function store(ModerWorkRequest $request)
    {
        if (!$request->hasFile('work_url')) {
            flash(__('messages.work-without-file'))->error();

            return redirect()->back();
        }

        $data['work_url'] = $this->upload($request, 'work_url', true);

        $data['thumbnail_url'] = $request->hasFile('thumbnail_url')
            ? $this->upload($request, 'thumbnail_url')
            : '';

        $work = Work::create([
            'title' => $request->input('title'),
            'description' => $request->input('description', ''),
            'work_url' => $data['work_url'],
            'lang' => $data['lang'],
            'thumbnail_url' => $data['thumbnail_url'],
            'user_id' => Auth::user()->id,
            'year' => date('Y'),
        ]);

        $work && $work->syncTerms($request->get('terms', []));

        $this->createFlashes($work, __('messages.work'));
        AdminLog::created('Work #'.$work->id);

        return $work
            ? redirect()->route('moder-work-edit', [
                'work' => $work,
            ])
            : $this->create();
    }

    public function edit(Work $work)
    {
        return $this->create($work);
    }

    public function update(Work $work, ModerWorkRequest $request)
    {
        $values = $request->only($work->getFillable());

        if ($request->hasFile('work_url')) {
            $values['work_url'] = $this->upload($request, 'work_url', true);
        }

        if ($request->hasFile('thumbnail_url')) {
            $values['thumbnail_url'] = $this->upload($request, 'thumbnail_url');
        }

        $work->syncTerms($request->get('terms', []));

        $this->updateFlashes($work->fill($values)->save(), __('messages.work'));
        AdminLog::updated('Work #'.$work->id);

        return $this->create($work);
    }

    public function generatePreview(Work $work)
    {
        GenerateWorkPreviewJob::dispatch($work)->onQueue('database');

        flash(__('forms.preview-added-to-queue'))->success();

        return redirect()->back();
    }

    public function remove(Work $work)
    {
        $this->deleteFlashes($work->delete(), __('messages.work'));
        AdminLog::deleted('Work #'.$work->id);

        return redirect()->route('moder-works');
    }

    public function ajaxValidatePlagiat(Work $work, Recaptcha $recaptcha, Request $request)
    {
        $error = '';
        $result = '';
        $proc = null;

        try {
            if ($recaptcha->isBot($request->get('token'))) {
                flash(__('forms.bot-detected'))->error();
            }

            CheckWorkPlagiarismJob::dispatch($work)->onQueue('database');

            flash(__('forms.work-added-to-check-queue'))->success();
        } catch (Exception $e) {
            flash($e->getMessage())->error();
        }

        return redirect()->back();
    }

    protected function upload(Request $request, $name, $secure = false)
    {
        $file = $request->file($name);

        return Files::upload($file, null, $secure);
    }
}
