<?php
namespace App\Http\Controllers\Moder;

use App\Abstractions\HasFlashesTrait;
use App\DownloadRequest;
use App\Http\Controllers\Controller;
use App\Status;
use Illuminate\Http\Request;

class DownloadRequestController extends Controller
{
    use HasFlashesTrait;

    public function index()
    {
        $requests = DownloadRequest::with('status')
            ->with('user')
            ->with('work')
            ->where('status_id', Status::PENDING)
            ->paginate(config('app.paginateItems'));

        return view('moder.download-request.list', [
            'requests' => $requests,
        ]);
    }

    public function remove(DownloadRequest $request)
    {
        $this->deleteFlashes($request->delete(), __('messages.download-request'));

        return redirect()->back();
    }

    public function update(DownloadRequest $request)
    {
        $request->status_id = Status::ACTIVE;
        $this->updateFlashes($request->save(), __('messages.download-request'));

        return redirect()->back();
    }
}
