<?php
namespace App\Http\Controllers\Moder;

use App\Abstractions\HasFlashesTrait;
use App\AdminLog;
use App\Comment;
use App\Http\Controllers\Controller;
use App\Http\Requests\CommentRequest;
use App\Mail\NewReviewMail;
use App\Status;
use App\Work;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    use HasFlashesTrait;

    public function index()
    {
        $comments = Comment::where('status_id', Status::PENDING)
            ->with('status')
            ->with('work')
            ->with('user')
            ->paginate(config('app.paginateItems'));

        return view('moder.comment.list', [
            'comments' => $comments,
            'statuses' => Status::all(),
        ]);
    }

    public function store(Work $work, CommentRequest $request, Comment $parent = null)
    {
        $comment = Comment::create([
            'text' => $request->input('text'),
            'user_id' => Auth::user()->id,
            'work_id' => $work->id,
            'status_id' => Status::PRIVATE,
        ]);

        if ($request->has('email')) {
            $this->mail($comment);
        }

        $this->createFlashes($comment, __('messages.comment'));

        AdminLog::created('Comment for work #'.$work->id);

        return redirect()->back();
    }

    protected function mail(Comment $comment)
    {
        try {
            if (empty($comment->work)) {
                return false;
            }

            if (empty($comment->work->user)) {
                return false;
            }

            Mail::to($comment->work->user->email)->send(new NewReviewMail($comment));
        } catch (Exception $e) {
            return [
                'result' => false,
                'message' => $e->getMessage(),
            ];
        }
    }

    public function update(Comment $comment, CommentRequest $request)
    {
        $values = $request->only($comment->getFillable());
        $this->updateFlashes($comment->fill($values)->save(), __('messages.comment'));

        AdminLog::updated('Comment #'.$comment->id);

        return redirect()->back();
    }

    public function remove(Comment $comment)
    {
        $this->deleteFlashes($comment->delete(), __('messages.comment'));

        AdminLog::deleted('Comment #'.$comment->id);

        return redirect()->back();
    }
}
