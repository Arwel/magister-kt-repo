<?php
namespace App\Http\Controllers\Moder;

use App\Abstractions\HasFlashesTrait;
use App\AdminLog;
use App\Http\Controllers\Controller;
use App\Http\Requests\TermRequest;
use App\Taxonomy;
use App\Term;
use App\Utils\Files;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class TermController extends Controller
{
    use HasFlashesTrait;

    public function index(Taxonomy $taxonomy)
    {
        $terms = $taxonomy->termsLocale()->paginate(config('app.paginateItems'));

        return view('moder.term.list', [
            'terms' => $terms,
            'taxonomy' => $taxonomy,
        ]);
    }

    public function create(Taxonomy $taxonomy, $term = null)
    {
        $route = $term
            ? route('moder-term-update', [
                'taxonomy' => $taxonomy,
                'term' => $term,
            ])
            : route('moder-term-store', [
                'taxonomy' => $taxonomy,
            ]);

        $terms = $term
            ? $taxonomy->terms()->where('id', '<>', $term->id)->get()
            : $taxonomy->terms;

        return view('moder.term.form', [
            'term' => $term,
            'taxonomy' => $taxonomy,
            'termOptions' => $terms,
            'route' => $route,
        ]);
    }

    public function store(Taxonomy $taxonomy, TermRequest $request)
    {
        $data = $request->only(['title', 'description', 'term_id']);

        $data['thumbnail_url'] = $request->hasFile('thumbnail_url')
            ? $this->uploadThumbnail($request)
            : '';
        $data['user_id'] = Auth::user()->id;
        $data['taxonomy_id'] = $taxonomy->id;
        $data['lang'] = App::getLocale();

        $term = Term::create($data);
        $this->createFlashes($term, __('messages.term'));

        AdminLog::created('Term #'.$term->id);

        return $term
            ? redirect()->route('moder-term-edit', [
                'taxonomy' => $taxonomy,
                'term' => $term,
            ])
            : $this->create($taxonomy);
    }

    public function edit(Taxonomy $taxonomy, Term $term)
    {
        return $this->create($taxonomy, $term);
    }

    public function update(Taxonomy $taxonomy, Term $term, TermRequest $request)
    {
        $values = $request->only($term->getFillable());
        $values['lang'] = App::getLocale();

        if ($request->hasFile('thumbnail_url')) {
            $values['thumbnail_url'] = $this->uploadThumbnail($request);
        }

        $this->updateFlashes($term->fill($values)->save(), __('messages.term'));
        AdminLog::updated('Term #'.$term->id);

        return $this->create($taxonomy, $term);
    }

    public function remove(Taxonomy $taxonomy, Term $term)
    {
        $this->deleteFlashes($term->delete(), __('messages.term'));
        AdminLog::removed('Term #'.$term->id);

        return redirect()->route('moder-terms', ['taxonomy' => $taxonomy]);
    }

    protected function uploadThumbnail(TermRequest $request)
    {
        $file = $request->file('thumbnail_url');

        return Files::upload($file);
    }
}
