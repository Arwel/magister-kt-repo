<?php
namespace App\Http\Controllers\Moder;

use App\Abstractions\HasFlashesTrait;
use App\AdminLog;
use App\Http\Controllers\Controller;
use App\Http\Requests\TaxonomyRequest;
use App\Taxonomy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class TaxonomyController extends Controller
{
    use HasFlashesTrait;

    public function index()
    {
        $taxonomies = Taxonomy::where('lang', App::getLocale())->paginate(config('app.paginateItems'));

        return view('moder.taxonomy.list', [
            'taxonomies' => $taxonomies,
        ]);
    }

    public function create($taxonomy = null)
    {
        $route = $taxonomy ? route('moder-taxonomy-update', [
            'taxonomy' => $taxonomy,
        ]) : route('moder-taxonomy-store');

        return view('moder.taxonomy.form', [
            'taxonomy' => $taxonomy,
            'route' => $route,
        ]);
    }

    public function store(TaxonomyRequest $request)
    {
        $data = $request->only(['title', 'description']);
        $data['lang'] = App::getLocale();
        $taxonomy = Taxonomy::create($data);

        $this->createFlashes($taxonomy, __('messages.taxonomy'));

        AdminLog::created('Taxonomy #'.$taxonomy->id);

        return $taxonomy
            ? redirect()->route('moder-taxonomy-edit', [
                'taxonomy' => $taxonomy,
            ])
            : $this->create();
    }

    public function edit(Taxonomy $taxonomy)
    {
        return $this->create($taxonomy);
    }

    public function update(Taxonomy $taxonomy, TaxonomyRequest $request)
    {
        $values = $request->only($taxonomy->getFillable());
        $values['lang'] = App::getLocale();

        $this->updateFlashes($taxonomy->fill($values)->save(), __('messages.taxonomy'));
        AdminLog::updated('Taxonomy #'.$taxonomy->id);

        return $this->create($taxonomy);
    }

    public function remove(Taxonomy $taxonomy)
    {
        $this->deleteFlashes($taxonomy->delete(), __('messages.taxonomy'));
        AdminLog::created('Taxonomy #'.$taxonomy->id);

        return redirect()->route('moder-taxonomies');
    }
}
