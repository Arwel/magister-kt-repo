<?php

namespace App\Http\Middleware;

use App\AccessRequest;
use App\DownloadRequest;
use App\Role;
use App\Status;
use App\Work;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class InitCounters
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if (!$user) {
            return $next($request);
        } else {
            $this->initUserCounters($user->id);
        }

        $role = $user->role;
        if ($role->can(Role::MODER_PRIORITY)) {
            $this->initModerCounters();
        }

        if ($role->can(Role::ADMIN_PRIORITY)) {
            $this->initAdminCounters();
        }

        return $next($request);
    }

    protected function initUserCounters($id)
    {
        $count = Work::where('status_id', Status::PENDING)
        ->where('user_id', $id)
        ->count();
        Cache::forever('myWorksCounter', (int)$count);
    }

    protected function initModerCounters()
    {
        $count = Work::where('status_id', Status::PENDING)->count();
        Cache::forever('worksCounter', (int)$count);

        $count = DownloadRequest::where('status_id', Status::PENDING)->count();
        Cache::forever('downloadRequestCounter', (int)$count);
    }

    protected function initAdminCounters()
    {
        $count = AccessRequest::where('invited', false)->count();
        Cache::forever('accessRequestsCounter', (int)$count);
    }
}
