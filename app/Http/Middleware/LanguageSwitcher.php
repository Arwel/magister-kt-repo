<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use Illuminate\Support\Facades\App;

class LanguageSwitcher
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $lang = $request->get('locale');
        if ($lang) {
            $request->session()->put('locale', $lang);

            return redirect()->back();
        } else {
            $lang = $request->session()->get('locale', config('app.locale'));
        }

        App::setLocale($lang);

        return $next($request);
    }
}
