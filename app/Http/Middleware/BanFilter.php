<?php

namespace App\Http\Middleware;

use App\Role;
use Closure;
use Illuminate\Support\Facades\Auth;

class BanFilter
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (!Auth::user()) {
            return $next($request);
        }

        $role = Auth::user()->role;
        if ($role instanceof Role && $role->priority == Role::BANNED_PRIORITY) {
            flash('Вибачте, але ви забанені')->error();
            Auth::logout();

            return redirect('home');
        }

        return $next($request);
    }
}
