<?php
namespace App\Http\Requests;

use App\Abstractions\PublicAjaxFormRequestTrait;
use Illuminate\Foundation\Http\FormRequest;

class ContactUsRequest extends FormRequest
{
    use PublicAjaxFormRequestTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'text' => 'required|string|max:1024',
            'token' => 'required|string|max:1024',
        ];
    }
}
