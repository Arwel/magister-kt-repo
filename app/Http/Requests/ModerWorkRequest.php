<?php
namespace App\Http\Requests;

use App\Abstractions\ModerFormRequestTrait;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;
use Illuminate\Validation\Validator;

class ModerWorkRequest extends FormRequest
{
    use ModerFormRequestTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:255',
            'lang' => 'required|string|size:2',
            'description' => 'sometimes|nullable|string|max:1024',
            'status_id' => 'required|integer|min:0',
        ];
    }
}
