<?php
namespace App\Http\Requests;

use App\Abstractions\ModerFormRequestTrait;
use App\Role;
use App\Utils\UserHelper;
use Illuminate\Foundation\Http\FormRequest;

class CommentRequest extends FormRequest
{
    use ModerFormRequestTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'text' => 'required|string|max:1024',
            'comment_id' => 'sometimes|nullable|integer|min:0',
        ];
    }
}
