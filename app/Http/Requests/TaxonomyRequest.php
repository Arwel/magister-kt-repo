<?php
namespace App\Http\Requests;

use App\Abstractions\ModerFormRequestTrait;
use App\Role;
use App\Utils\UserHelper;
use Illuminate\Foundation\Http\FormRequest;

class TaxonomyRequest extends FormRequest
{
    use ModerFormRequestTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:255',
            'description' => 'required|string|max:500',
        ];
    }
}
