<?php
namespace App\Http\Requests;

use App\Abstractions\PublicAjaxFormRequestTrait;
use Illuminate\Foundation\Http\FormRequest;

class InviteRequest extends FormRequest
{
    use PublicAjaxFormRequestTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255|unique:access_requests',
            'email' => 'required|string|email|max:255|unique:access_requests',
        ];
    }
}
