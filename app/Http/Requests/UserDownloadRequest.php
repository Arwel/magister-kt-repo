<?php

namespace App\Http\Requests;

use App\Abstractions\UserFormRequestTrait;
use Illuminate\Foundation\Http\FormRequest;

class UserDownloadRequest extends FormRequest
{
    use UserFormRequestTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'work_id' => 'required|number|min:0',
            'status_id' => 'sometimes|nullable|number|min:0',
        ];
    }
}
