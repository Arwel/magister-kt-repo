<?php
namespace App\Http\Requests;

use App\Abstractions\UserFormRequestTrait;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;
use Illuminate\Validation\Validator;

class UserWorkRequest extends FormRequest
{
    use UserFormRequestTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:255',
            'description' => 'sometimes|nullable|string|max:1024',
            'terms.*.*' => 'sometimes|nullable|integer|min:0',
        ];
    }
}
