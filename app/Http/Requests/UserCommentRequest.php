<?php

namespace App\Http\Requests;

use App\Abstractions\UserFormRequestTrait;
use Illuminate\Foundation\Http\FormRequest;

class UserCommentRequest extends FormRequest
{
    use UserFormRequestTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'text' => 'required|string',
            'comment_id' => 'sometimes|nullable|integer',
        ];
    }
}
