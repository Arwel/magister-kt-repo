<?php

namespace App;

use App\File;
use App\User;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Term extends Model
{
    protected $fillable = [
        'title',
        'slug',
        'description',
        'thumbnail_url',
        'lang',
        'term_id',
        'taxonomy_id',
    ];

    use Sluggable;

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title',
            ],
        ];
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function term()
    {
        return $this->belongsTo(Term::class);
    }

    public function taxonomy()
    {
        return $this->belongsTo(Taxonomy::class);
    }

    public function works()
    {
        return $this->belongsToMany(Work::class, 'work_term');
    }

    public function getThumbnailUrl()
    {
        return config('app.url').'/'.$this->thumbnail_url;
    }

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($term) {
            $term->term()->delete();
        });
    }
}
