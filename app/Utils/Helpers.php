<?php
namespace App\Utils;

class Helpers
{
    public static function cropText($text)
    {
        if (strlen($text) > 75) {
            return substr($text, 0, 75) . '...';
        }

        return $text;
    }
}
