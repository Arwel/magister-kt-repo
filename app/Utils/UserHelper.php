<?php
namespace App\Utils;

use App\User;
use Illuminate\Support\Facades\Auth;

class UserHelper
{
    public static function can($priority, User $user = null)
    {
        $user = static::getUser($user);

        return $user && $user->role && $user->role->can($priority);
    }

    public static function canOnly($priority, User $user = null)
    {
        $user = static::getUser($user);

        return $user && $user->role && $user->role->priority == $priority;
    }

    protected static function getUser($user)
    {
        return $user ?: Auth::user();
    }
}
