<?php
namespace App\Utils;

use App\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class Files {
    public static function resolvePath(array $path)
    {
        return implode(DIRECTORY_SEPARATOR, $path);
    }

    public static function urlToPath($diskName, $fileName)
    {
        $disk = Files::getDisk($diskName);

        return str_replace($disk->url(''), $disk->path(''), $fileName);
    }

    public static function pathToUrl($diskName, $fileName)
    {
        $disk = Files::getDisk($diskName);

        return str_replace($disk->path(''), $disk->url(''), $fileName);
    }

    public static function getDefaultPath()
    {
        return static::resolvePath([date('Y'), date('n'), date('j')]);
    }

    public static function upload(UploadedFile $file, $path = null, $private = false)
    {
        if (empty($path)) {
            $path = static::getDefaultPath();
        }

        $diskName = $private ? static::getPrivateDiskName() :  static::getDiskName();
        $disk = static::getDisk($diskName);
        $fileName = $file->getClientOriginalName();

        while($disk->exists($path . $fileName)) {
            $fileName = time() . '.' . $fileName;
        }

        return $file->isValid()
            ? str_replace(config('app.url').'/', '', $disk->url($file->storeAs($path, $fileName, $diskName)))
            : false;
    }

    public static function getDiskName()
    {
        return config('app.filesDisk', 'public');
    }

    public static function getPrivateDiskName()
    {
        return config('app.privateDisk', 'public');
    }

    public static function getDisk($diskName = null)
    {
        if (empty($diskName)) {
            $diskName = static::getDiskName();
        }

        return Storage::disk($diskName);
    }
}
