<?php
namespace App\Utils;

use Exception;

class Color
{
    const TRANSFORM_LIGHTEN = 0x0032;
    const TRANSFORM_DARKEN = 0x0016;

    public static function isDark($color)
    {
        return static::is($color);
    }

    public static function isLight($color)
    {
        return static::is($color, false);
    }

    public static function generateHex()
    {
        $parts = [0, 1, 2];
        foreach ($parts as &$part) {
            $part = dechex(rand(0, 255));
        }

        return '#' . implode('', $parts);
    }

    public static function generateRgb()
    {
        $parts = [0, 1, 2];
        foreach ($parts as &$part) {
            $part = rand(0, 255);
        }

        return 'rgb(' . implode(',', $parts) . ')';
    }

    /**
     * @param      $color
     * @param float $bareer
     * @param bool $isDark
     * @return bool
     */
    public static function is($color, $isDark = true, $bareer = 382.5)
    {
        if (empty($color)) {
            return $isDark;
        }

        $rgb = static::prepareRgb($color);
        $sum = array_sum($rgb);

        return $isDark ? $sum < $bareer : $sum > $bareer;
    }

    /**
     * @param $color
     *
     * @return bool|null|string
     */
    protected static function getColorModel($color)
    {
        $colorPalette = null;
        $bracketPos = strpos($color, '(');
        $colorPalette = substr($color, 0, $bracketPos);
        if ($bracketPos !== false) {
            return $colorPalette;
        } elseif ($color[0] === '#') {
            return 'hex';
        }

        return false;
    }

    /**
     * @param $color
     *
     * @return array
     */
    protected static function rgb($color)
    {
        $colorValue = explode(',', $color);
        if (sizeof($colorValue) > 3) {
            $colorValue = array_slice($colorValue, 0, 3);
        }
        foreach ($colorValue as &$rgbPart) {
            $rgbPart = filter_var(
                $rgbPart,
                FILTER_SANITIZE_NUMBER_FLOAT,
                FILTER_FLAG_ALLOW_FRACTION
            );
            if ($rgbPart > 255) {
                $rgbPart = 255;
            } elseif ($rgbPart < 0) {
                $rgbPart = 0;
            }
        }

        return $colorValue;
    }

    /**
     * @param $color
     *
     * @return string
     */
    public static function toRgb($color)
    {
        try {
            $rgb = static::prepareRgb($color);
            $alpha = static::getAlpha($color);
            $pattern = $alpha == 1
            ? 'rgb(%s)'
            : 'rgba(%s, %f)';

            return sprintf($pattern, implode(', ', $rgb), $alpha);
        } catch (Exception $ex) {
            return '';
        }
    }

    /**
     * @param $color
     *
     * @return string
     */
    public static function toHex($color)
    {
        try {
            $alpha = 100 - static::getAlpha($color) * 100;
            $light = static::transform(
                $color,
                $alpha,
                static::TRANSFORM_LIGHTEN
            );
            $rgb = static::prepareRgb($light);
            $hex = '';
            foreach ($rgb as $rgbPart) {
                $hex .= dechex($rgbPart);
            }

            return '#' . $hex;

        } catch (Exception $ex) {
            return '';
        }
    }

    /**
     * @param $color
     *
     * @return array
     */
    protected static function rgba($color)
    {
        return static::rgb($color);
    }

    /**
     * @param $color
     *
     * @return array
     */
    protected static function hex($color)
    {
        $hex = null;
        preg_match('/#[0-9a-fA-F]{6}|#[0-9a-fA-F]{3}/i', trim($color), $hex);
        $hex = static::sanitizeMatches($hex);
        $hex = substr($hex, 1, 6);
        $colorLen = intval(strlen($hex) / 3);
        $hexColorValue = str_split($hex, $colorLen);
        foreach ($hexColorValue as &$hexColorPart) {
            if (strlen($hexColorPart) == 1) {
                $hexColorPart .= $hexColorPart;
            }
            $hexColorPart = hexdec($hexColorPart);
        }

        return $hexColorValue;
    }

    /**
     * @param $color
     *
     * @return int|mixed
     */
    protected static function getAlpha($color)
    {
        $colorValue = explode(',', $color);

        return isset($colorValue[3])
           ? static::sanitizeAlpha($colorValue[3])
           : 1;
    }

    /**
     * @param $color
     *
     * @return mixed
     */
    protected static function prepareRgb($color)
    {
        $colorPalette = static::getColorModel($color);

        return static::$colorPalette($color);
    }

    /**
     * @param $alpha
     *
     * @return int|mixed
     */
    protected static function sanitizeAlpha($alpha)
    {
        $alpha = filter_var(
            $alpha,
            FILTER_SANITIZE_NUMBER_FLOAT,
            FILTER_FLAG_ALLOW_FRACTION
        );
        if ($alpha <= 1 && $alpha >= 0) {
            return $alpha;
        } elseif ($alpha < 0) {
            return 0;
        }

        return 1;
    }

    /**
     * @param $color
     * @param $alpha
     *
     * @return string
     */
    public static function setAlpha($color, $alpha)
    {
        $rgb = static::prepareRgb($color);
        $alpha = static::sanitizeAlpha($alpha);

        return 'rgba(' . implode(', ', $rgb) . ', ' . $alpha . ')';
    }

    /**
     * @param     $color
     * @param     $percent
     * @param int $mode
     *
     * @return bool|string
     */
    public static function transform($color, $percent, $mode = self::TRANSFORM_DARKEN) {
        if (empty($color) || !is_string($color)) {
            return $color;
        }
        $rgb = static::prepareRgb($color);
        $alpha = static::getAlpha($color);
        $percent = static::sanitizePercent($percent);
        $delta = 255 * $percent / 100;
        foreach ($rgb as &$rgbPart) {
            $newPart = $mode == self::TRANSFORM_DARKEN
                ? $rgbPart - $delta
                : $rgbPart + $delta;
            $rgbPart = static::sanitizeRgb($newPart);
        }

        return 'rgba(' . implode(', ', $rgb) . ', ' . $alpha . ')';
    }

    /**
     * @param $percent
     *
     * @return int|mixed
     */
    protected static function sanitizePercent($percent)
    {
        if (is_string($percent)) {
            $percent = filter_var(
                $percent,
                FILTER_SANITIZE_NUMBER_FLOAT,
                FILTER_FLAG_ALLOW_FRACTION
            );
        }
        if ($percent < 0) {
            $percent = 0;
        } elseif ($percent > 100) {
            $percent = 100;
        }

        return $percent;
    }

    /**
     * @param $color
     *
     * @return int|string
     */
    protected static function sanitizeRgb($color)
    {
        if (!is_numeric($color)) {
            return 0;
        } elseif ($color > -1 && $color < 256) {
            return round(trim($color));
        } elseif ($color > 255) {
            return 255;
        }

        return 0;
    }

    /**
     * @param $matches
     *
     * @return null
     */
    protected static function sanitizeMatches($matches)
    {
        if (!empty($matches[0])) {
            return $matches[0];
        }

        return null;
    }
}
