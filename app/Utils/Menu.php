<?php
namespace App\Utils;

use Illuminate\Support\Facades\Request;

class Menu
{
    protected static $routeName;

    protected static function getRouteName()
    {
        if (empty(static::$routeName)) {
            static::$routeName = Request::route()->getName();
        }

        return static::$routeName;
    }

    public static function active($checkName, $strict = true)
    {
        $currentRoute = static::getRouteName();

        $predicate = $strict
            ? $currentRoute == $checkName
            : strpos($currentRoute, $checkName) !== false;

        return $predicate
            ? 'active'
            : '';
    }
}
