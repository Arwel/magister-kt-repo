<?php
namespace App\Utils;

use Illuminate\Database\Eloquent\Collection;

class Form
{
    public static function ajaxStart($action, $class = '', $method = 'POST', array $atts = [])
    {
        $atts['data-form-type'] = 'ajax';

        return view('admin.form.start', [
            'action' => $action,
            'class' => $class,
            'method' => $method,
            'atts' => $atts,
        ]);
    }

    public static function start($action, $class = '', $method = 'POST', array $atts = [])
    {
        return view('admin.form.start', [
            'action' => $action,
            'class' => $class,
            'method' => $method,
            'atts' => $atts,
        ]);
    }

    public static function end()
    {
        return view('admin.form.end');
    }

    public static function submit($text, $class = '')
    {
        return view('admin.form.submit', [
            'text' => $text,
            'class' => $class,
        ]);
    }

    public static function file(
        $name,
        $label,
        $model,
        $class = 'form-control',
        $wrapperClass = 'form-group'
    ) {
        $value = $model ? $model->$name : old($name);

        return view('admin.form.file', [
            'name' => $name,
            'label' => $label,
            'value' => $value,
            'class' => $class,
            'wrapperClass' => $wrapperClass,
        ]);
    }

    public static function hidden($name, $value)
    {
        return view('admin.form.hidden', [
            'name' => $name,
            'value' => $value,
        ]);
    }

    public static function input(
        $name,
        $label,
        $model,
        $fillable = true,
        $type = 'text',
        $class = 'form-control',
        $wrapperClass = 'form-group',
        $placeholder = ''
    ) {
        $value = '';
        if ($fillable) {
            $value = $model ? $model->$name : old($name);
        }

        return view('admin.form.input', [
            'name' => $name,
            'label' => $label,
            'value' => $value,
            'type' => $type,
            'class' => $class,
            'wrapperClass' => $wrapperClass,
            'placeholder' => $placeholder,
        ]);
    }

    public static function textarea(
        $name,
        $label,
        $model,
        $rows = 10,
        $class = 'form-control',
        $wrapperClass = 'form-group'
    ) {
        return view('admin.form.textarea', [
            'value' => $model ? $model->$name : old($name),
            'name' => $name,
            'label' => $label,
            'rows' => $rows,
            'class' => $class,
            'wrapperClass' => $wrapperClass,
        ]);
    }

    public static function select(
        $name,
        $label,
        $options,
        $value,
        $withEmpty = false,
        $multiple = false,
        $class = 'form-control',
        $wrapperClass = 'form-group'
    ) {
        return view('admin.form.select', [
            'name' => $name,
            'label' => $label,
            'options' => static::getFormSelectData($options, $value, $withEmpty, $multiple),
            'multiple' => $multiple,
            'class' => $class,
            'wrapperClass' => $wrapperClass,
        ]);
    }

    public static function getFormSelectData(Collection $items, $value, $withEmpty = false, $multiple = false)
    {
        if ($multiple && !is_array($value)) {
            $value = [];
        }

        $options = [];
        if ($withEmpty) {
            $options[' '] = [
                'title' => '-',
                'selected' => false,
            ];
        }

        foreach ($items as $item) {
            $options[$item->id] = [
                'title' => $item->title,
                'selected' => static::isSelected($item->id, $value, $multiple),
            ];
        }

        return $options;
    }

    protected static function isSelected($id, $value, $multiple)
    {
        return $multiple
            ? in_array($id, $value)
            : $id == $value;
    }

    public static function jsFields($options)
    {
        return view('admin.form.js', [
            'options' => $options,
        ]);
    }
}
