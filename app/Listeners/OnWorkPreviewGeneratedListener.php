<?php
namespace App\Listeners;

use App\Events\OnWorkPreviewGenerated;
use App\Utils\Files;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Intervention\Image\Facades\Image;

class OnWorkPreviewGeneratedListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OnWorkPreviewGenerated  $event
     * @return void
     */
    public function handle(OnWorkPreviewGenerated $event)
    {
        $path = $event->path;

        Image::make($path)
        ->insert(Files::resolvePath([
            base_path(),
            'resources',
            'assets',
            'img',
            'watermark.png',
        ]))->save($path);
    }
}
