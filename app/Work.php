<?php
namespace App;

use App\Abstractions\HasDateFormatterTrait;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Work extends Model
{
    use Sluggable;
    use HasDateFormatterTrait;

    protected $fillable = [
        'title',
        'slug',
        'description',
        'work_url',
        'lang',
        'plagiarism_percent',
        'thumbnail_url',
        'status_id',
        'user_id',
        'year',
    ];

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function terms()
    {
        return $this->belongsToMany(Term::class, 'work_term');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function previews()
    {
        return $this->hasMany(Preview::class);
    }

    public function downloadRequests()
    {
        return $this->hasMany(DownloadRequest::class);
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title',
            ],
        ];
    }

    public function getTermsId()
    {
        $ids = [];
        foreach ($this->terms as $term) {
            $ids[] = $term->id;
        }

        return $ids;
    }

    public function getTermsLinks()
    {
        $terms = [];
        foreach ($this->terms()->with('taxonomy')->get() as $term) {
            $key = $term->taxonomy->title;
            if (empty($terms[$key])) {
                $terms[$key] = [];
            }

            $terms[$key][] = '<a href="'.route('public-works-term', ['term' => $term]).'">'.$term->title.'</a>';
        }

        return $terms;
    }

    public function syncTerms(array $terms)
    {
        $_terms = [];
        foreach ($terms as $term) {
            $_terms = array_merge($_terms, $term);
        }

        return $this->terms()->sync($_terms);
    }

    public function getThumbnailUrl()
    {
        if (empty($this->thumbnail_url)) {
            return asset('assets/images/doc-placeholder.jpg');
        }

        return config('app.url').'/'.$this->thumbnail_url;
    }

    public function getWorkUrl()
    {
        return config('app.url').'/'.$this->work_url;
    }

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($work) {
            $work->comments()->delete();
            $work->downloadRequests()->delete();
            $work->previews()->delete();
        });
    }
}
