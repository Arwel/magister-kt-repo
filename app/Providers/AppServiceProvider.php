<?php
namespace App\Providers;

use App\AccessRequest;
use App\Jobs\AvatarGenerateJob;
use ConvertApi\ConvertApi;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use LasseRafn\InitialAvatarGenerator\InitialAvatar;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        ConvertApi::setApiSecret(config('app.converterApiKey'));

        Blade::directive('crop', function ($text) {
            return "<?php echo App\\Utils\\Helpers::cropText($text); ?>";
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(InitialAvatar::class, function () {
            return new InitialAvatar;
        });

        $this->app->singleton(Client::class, function () {
            return new Client;
        });
    }
}
