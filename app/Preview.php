<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Preview extends Model
{
    protected $fillable = [
        'work_id',
        'url',
    ];

    public function work()
    {
        return $this->belongsTo(Work::class);
    }
}
