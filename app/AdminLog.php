<?php
namespace App;

use App\Abstractions\HasDateFormatterTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class AdminLog extends Model
{
    const DEBUG = 'DEBUG';
    const ERROR = 'ERROR';
    const ACCESS_ATTEMPT = 'ACCESS_ATTEMPT';
    const WARNING = 'WARNING';
    const REMOVED = 'REMOVED';
    const UPDATED = 'UPDATED';
    const CREATED = 'CREATED';

    use HasDateFormatterTrait;

    protected $fillable = [
        'tag',
        'description',
        'user_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    protected static function add($tag, $description)
    {
        return static::create([
            'tag' => $tag,
            'description' => $description,
            'user_id' => Auth::user()->id,
        ]);
    }

    public static function debug($description)
    {
        return static::add(static::DEBUG, $description);
    }

    public static function error($description)
    {
        return static::add(static::ERROR, $description);
    }

    public static function access($description)
    {
        return static::add(static::ACCESS_ATTEMPT, $description);
    }

    public static function warning($description)
    {
        return static::add(static::WARNING, $description);
    }

    public static function removed($description)
    {
        return static::add(static::REMOVED, $description);
    }

    public static function updated($description)
    {
        return static::add(static::UPDATED, $description);
    }

    public static function created($description)
    {
        return static::add(static::CREATED, $description);
    }
}
