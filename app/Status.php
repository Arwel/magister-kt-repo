<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    const ACTIVE = 1;
    const PENDING = 2;
    const DELETED = 3;
    const BANNED = 4;
    const PRIVATE = 5;
    const REJECTED = 6;

    public function works()
    {
        return $this->hasMany(Work::class);
    }
}
