<?php

namespace App\Jobs;

use App\Events\OnWorkPreviewGenerated;
use App\Utils\Files;
use App\Work;
use ConvertApi\ConvertApi;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Intervention\Image\Facades\Image;

class GenerateWorkPreviewJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $work;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Work $work)
    {
        $this->work = $work;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $work = $this->work;

        $path = Files::urlToPath(Files::getPrivateDiskName(), $work->work_url);
        $disk = Files::getDisk(Files::getPrivateDiskName());
        $result = ConvertApi::convert('png', ['File' => $path]);
        $destPath = Files::resolvePath([Files::getDefaultPath(), $work->id]);

        $appUrl = config('app.url').'/';
        $previews = [];
        if(isset($result->response['Files']) && is_array($result->response['Files'])) {
            foreach ($result->response['Files'] as $file) {
                if (empty($file['Url'])) {
                    continue;
                }

                $content = file_get_contents($file['Url']);
                $filePath = Files::resolvePath([$destPath, basename($file['Url'])]);
                $disk->put($filePath, $content, 'public');

                $path = $disk->path($filePath);

                $this->addWaterMark($path);

                $previews[] = [
                    'work_id' => $work->id,
                    'url' => str_replace($appUrl, '', $disk->url($filePath)),
                ];
            }

            $work->previews()->insert($previews);
        }
    }

    protected function addWaterMark($path)
    {
        try {
            Image::make($path)
            ->insert(Files::resolvePath([
                base_path(),
                'resources',
                'assets',
                'img',
                'watermark.png',
            ]))->save($path);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}
