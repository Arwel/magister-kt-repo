<?php

namespace App\Jobs;

use App\Http\Controllers\Moder\CopyleaksConfig;
use App\Utils\Files;
use App\Work;
use Copyleaks\CopyleaksCloud;
use Copyleaks\Products;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CheckWorkPlagiarismJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $work;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Work $work)
    {
        $this->work = $work;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $work = $this->work;

        $error = '';
        $result = '';
        $proc = null;

        try {
            $cloud = new CopyleaksCloud(config('app.plagiatEmail'), config('app.plagiatApiKey'), Products::Education);
            if(!isset($cloud->loginToken) || !$cloud->loginToken->validate()) {
                throw new Exception('Wrong credentials');
            }

            $proc = $cloud->createByFile(
                Files::urlToPath(Files::getPrivateDiskName(), $work->work_url),
                [
                    CopyleaksConfig::SANDBOX_MODE_HEADER,
                ]
            );

            $sleepsCount = 0;
            while ($proc->getStatus() != 100 && $sleepsCount++ < 10) {
                sleep(1);
            }

            $resp = $proc->getResult();
            $report = $cloud->getResultComparisonReport($resp[0]);

            $copied = !empty($report['TotalCopiedWords'])
                ? $report['TotalCopiedWords']
                : 0;
            $total = !empty($report['TotalWords'])
                ? $report['TotalWords']
                : 0;

            if ($total < 1) {
                throw new Exception(__('forms.empty-validation'));
            }

            $result = round($copied * 100 / $total, 2);
            $work->plagiarism_percent = (float)$result;
            $work->save();
        } catch (Exception $e) {
             echo $e->getMessage();
        } finally {
            if ($proc) {
                $proc->delete();
            }
        }
    }
}
