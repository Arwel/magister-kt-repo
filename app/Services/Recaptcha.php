<?php
namespace App\Services;

use GuzzleHttp\Client;

class Recaptcha
{
    const THREESHOLD = .7;

    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function request($token)
    {
        $response = $this->client->request('POST', 'https://www.google.com/recaptcha/api/siteverify', [
            'form_params' => [
                'secret' => config('app.recaptchaApiKey'),
                'response' => $token,
            ],
        ]);

        $json = json_decode($response->getBody()->getContents(), true);

        return !empty($json['score']) ? $json['score'] : 0;
    }

    public function isBot($token)
    {
        $score = $this->request($token);

        return $score <= static::THREESHOLD;
    }

    public function botResponse()
    {
        return response([
            'title' => base64_encode(__('forms.bot-detected-title')),
            'content' => base64_encode(__('forms.bot-detected')),
        ]);
    }
}
