<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Password;

class AccessRequest extends Model
{
    protected $fillable = [
        'name',
        'email',
        'token',
    ];

    public static function getNewToken()
    {
        $token = '';
        do {
            $token = Password::getRepository()->createNewToken();
        } while ((bool)static::where('token', $token)->first());

        return $token;
    }
}
