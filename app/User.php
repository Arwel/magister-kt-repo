<?php

namespace App;

use App\DownloadRequest;
use App\File;
use App\Role;
use App\Taxonomy;
use App\Utils\Color;
use App\Utils\Files;
use App\Work;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use LasseRafn\InitialAvatarGenerator\InitialAvatar;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'username',
        'avatar',
        'role_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function works()
    {
        return $this->hasMany(Work::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function downloadRequests()
    {
        return $this->hasMany(DownloadRequest::class);
    }

    public function hasDownloadRequest(Work $work)
    {
        return (bool)$this->getDownloadrequest($work);
    }

    public function canDownload(Work $work)
    {
        return (bool)$this->downloadRequests()->where('work_id', (int)$work->id)->where('status_id', Status::ACTIVE)->first();
    }

    public function getDownloadrequest(Work $work)
    {
        return $this->downloadRequests()->where('work_id', (int)$work->id)->first();
    }

    public function updateRole(Role $role)
    {
        if ($this->role != $role) {
            $role->users()->save($this);
        }
    }

    public function getAvatar()
    {
        $avatarUrl = $this->avatar;
        $avatarUrl = empty($avatarUrl) ? '#' : $avatarUrl;

        return config('app.url').'/'.$avatarUrl;
    }

    public function generateAvatar()
    {
        $bgColor = Color::generateHex();
        $fgColor = Color::isDark($bgColor)
            ? '#fff'
            : '#000';

        $avatar = app()->make(InitialAvatar::class);
        $stream = $avatar->name($this->name)
                ->length(1)
                ->fontSize(0.5)
                ->size(96)
                ->background($bgColor)
                ->color($fgColor)
                ->generate()
                ->stream('png');

        $disk = Files::getDisk();
        $path = Files::resolvePath([Files::getDefaultPath(), 'avatar.' . $this->name . '.png']);
        $disk->put($path, $stream);
        $url = $disk->url($path);
        $url = str_replace(config('app.url').'/', '', $url);

        $this->avatar = $url;
        $this->save();

        return $url;
    }

    public function getLabel()
    {
        $role = $this->role;

        if ($role->can(Role::ADMIN_PRIORITY)) {
            return '[A]';
        } elseif ($role->can(Role::MODER_PRIORITY)) {
            return '[M]';
        } elseif ($role->priority == Role::BANNED_PRIORITY) {
            return '[B]';
        }

        return '';
    }

    public function canEdit(Work $work)
    {
        return $this->id == $work->user_id || $this->role->can(Role::MODER_PRIORITY);
    }

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($user) {
            $user->works()->delete();
            $user->comments()->delete();
            $user->downloadRequests()->delete();
        });
    }
}
