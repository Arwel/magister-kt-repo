<?php
namespace App;

use App\Status;
use App\User;
use App\Work;
use Illuminate\Database\Eloquent\Model;

class DownloadRequest extends Model
{
    protected $fillable = [
        'user_id',
        'work_id',
        'status_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function work()
    {
        return $this->belongsTo(Work::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function canDownload(Work $work, User $user)
    {
        return $this->work_id == $work->id && $user->id == $this->user_id && $this->status_id == Status::ACTIVE;
    }
}
