<?php
namespace App\Abstractions;

use App\Role;
use App\Utils\UserHelper;

trait ModerFormRequestTrait
{
    public function authorize()
    {
        return UserHelper::can(Role::MODER_PRIORITY);
    }
}
