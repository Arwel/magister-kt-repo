<?php
namespace App\Abstractions;

use Illuminate\Support\Carbon;

trait HasDateFormatterTrait
{
    public function getCreatedDateFormatted()
    {
        return Carbon::parse($this->created_at)->format(config('app.dateFormat'));
    }
}
