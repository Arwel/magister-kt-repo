<?php
namespace App\Abstractions;

use App\Role;
use App\Utils\UserHelper;

trait UserFormRequestTrait
{
    public function authorize()
    {
        return UserHelper::can(Role::USER_PRIORITY);
    }
}
