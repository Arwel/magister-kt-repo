<?php
namespace App\Abstractions;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\App;

trait HasLangValidateTrait
{
    public function checkLang(string $lang): string
    {
        return in_array($lang, config('app.supportedLanguages')) ? $lang : App::getLocale();
    }
}
