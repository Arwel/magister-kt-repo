<?php
namespace App\Abstractions;

use Illuminate\Database\Eloquent\Model;

trait HasFlashesTrait
{
    public function createFlashes($isCreated, $name = null)
    {
        if (empty($name)) {
            $name = __('messages.item');
        }

        if ($isCreated) {
            flash(__('messages.success-create', [
                'name' => $name,
            ]))->success();
        } else {
            flash(__('messages.error-create', [
                'name' => $name,
            ]))->error();
        }
    }

    public function updateFlashes($isUpdated, $name = null)
    {
        if (empty($name)) {
            $name = __('messages.item');
        }

        if ($isUpdated) {
            flash(__('messages.success-edit', [
                'name' => $name,
            ]))->success();
        } else {
            flash(__('messages.error-edit', [
                'name' => $name,
            ]))->error();
        }
    }

    public function deleteFlashes($isDeleted, $name = null)
    {
        if (empty($name)) {
            $name = __('messages.item');
        }

        if ($isDeleted) {
            flash(__('messages.success-delete', [
                'name' => $name,
            ]))->success();
        } else {
            flash(__('messages.error-delete', [
                'name' => $name,
            ]))->error();
        }
    }
}
