<?php
namespace App\Abstractions;

trait PublicAjaxFormRequestTrait
{
    public function validate()
    {
        $instance = $this->getValidatorInstance();

        return !$instance->fails();
    }

    public function getValidator()
    {
        return $this->getValidatorInstance();
    }

    public function authorize()
    {
        return true;
    }
}
